﻿/***********************************************************************************
Description:      
As of 08 Aug 2017 this table is not in use.  The purpose of this table is to handle
what were previously called "fudge" factors.  Numbers used to sway EGC results that
are not aligning with the model.

Author: Rodrick Blanton
Date: 03 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/



	
	
CREATE TABLE dbo.EGCCRVBias
(
	crvGroupID VARCHAR(50) NOT NULL
	,crvSizeGroup VARCHAR(50) NOT NULL
	,studyYear INT DEFAULT(1900) NOT NULL
	,isDiviser BIT DEFAULT(1) NOT NULL
	,bias REAL DEFAULT(1) NOT NULL
)