﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************
Description:      
Get CRV Fuel Group by CRV Group or CRV Size Group

Author: Rodrick Blanton
Date: 28 NOV 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/
ALTER FUNCTION dbo.GetCRVFuelGroup
(	
	-- Add the parameters for the function here
	@variable VARCHAR(50)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT TOP 1 crvFuelGroup
	FROM Power.dbo.CRVGroups
	WHERE crvGroupID = @variable
	OR crvSizeGroupID = @variable
)
GO
