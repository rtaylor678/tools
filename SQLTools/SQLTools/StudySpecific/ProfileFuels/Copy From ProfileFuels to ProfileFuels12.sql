USE ProfileFuels12
GO

/*
SELECT DISTINCT RefineryID, DataSet FROM ProfileFuels.dbo.Submissions o WHERE Submitted > '1/1/2014' AND NOT EXISTS (SELECT * FROM ProfileFuels12.dbo.Submissions n WHERE n.RefineryID = o.RefineryID AND n.DataSet = o.DataSet)
RefineryID	Dataset	CoLoc
192NSA	Actual	ANCAP - LA TEJA
172NSA	Actual	ECOPETROL  - BARRANCABERMEJA
57NSA 	Actual	HUSKY - LIMA
46NSA 	ACTUAL	NCRA - MCPHERSON
142NSA	Actual	PEMEX - CADEREYTA
143NSA	Actual	PEMEX - MADERO
144NSA	Actual	PEMEX - MINATITLAN
145NSA	Actual	PEMEX - SALAMANCA
133NSA	Actual	PEMEX - SALINA CRUZ
132NSA	Actual	PEMEX - TULA
190EUR	Actual	QATAR PETROLEUM - MESAIEED
*/

DECLARE @RefineryID varchar(6), @Dataset varchar(15), @EarliestDate smalldatetime
SELECT @RefineryID = 'XXPAC', @Dataset = 'Actual', @EarliestDate = '1/1/1980'

EXEC spDeleteDataset @RefineryID, @Dataset

INSERT TSort (RefineryID, Company, Location, CoLoc, Study, Country, State, Class, RSC, PADD, Continent, Area, CoastalInland, MultiRef, RVLocFactor, InflFactor, HydroInvInt, SpecProd, RefOwn, RefOper, CompanyID, FuelsLubesCombo, RefineryPwd, PwdSaltKey)
SELECT RefineryID, Company, Location, CoLoc, Study, Country, State, Class, RSC, PADD, Continent, Area, CoastalInland, MultiRef, RVLocFactor, InflFactor, HydroInvInt, SpecProd, RefOwn, RefOper, CompanyID, FuelsLubesCombo, RefineryPwd, PwdSaltKey
FROM ProfileFuels.dbo.TSort WHERE RefineryID = @RefineryID

INSERT CurrenciesToCalc (RefineryID, Currency)
SELECT RefineryID, Currency
FROM ProfileFuels.dbo.CurrenciesToCalc WHERE RefineryID = @RefineryID

INSERT DefUnitFactorData (RefineryID, UnitID, ProcessID, Conv, ReactorPress, Coke, FeedGravity, FeedConcarbon, FeedCrack, FeedAniline, FeedASTM10, FeedASTM90, FeedH2, FeedC3Olefin, FeedC4Olefin, FeedC5Olefin, FeedTotalOlefin, FeedNap, FeedArom, Nap2Arom, ProdRONC, ProdDiesel, ProdHGO, ProdHvHycCrk, ProdLtHycCrk, ProdBal, LN_H2Cons, LN_SolventRatio, LN_FeedCap, LN_OperPcnt, EIILNFraction, MN_H2Cons, MN_SolventRatio, MN_FeedCap, MN_OperPcnt, EIIMNFraction, HN_H2Cons, HN_SolventRatio, HN_FeedCap, HN_OperPcnt, EIIHNFraction, BRS_H2Cons, BRS_SolventRatio, BRS_FeedCap, BRS_OperPcnt, EIIBRSFraction, EIIRefWaxPcnt, EIIMicroWaxPcnt, FeedDensity, BtmProdPcnt, FZTemp, FZPress, EIIVGODesulf, EIIDieselDesulf, EIIVGOtoLt, EIIDieselToLt, H2Cons, SolventRatio, SumTempDiff, Yield, TotalFeed, VGOFeed)
SELECT RefineryID, UnitID, ProcessID, Conv, ReactorPress, Coke, FeedGravity, FeedConcarbon, FeedCrack, FeedAniline, FeedASTM10, FeedASTM90, FeedH2, FeedC3Olefin, FeedC4Olefin, FeedC5Olefin, FeedTotalOlefin, FeedNap, FeedArom, Nap2Arom, ProdRONC, ProdDiesel, ProdHGO, ProdHvHycCrk, ProdLtHycCrk, ProdBal, LN_H2Cons, LN_SolventRatio, LN_FeedCap, LN_OperPcnt, EIILNFraction, MN_H2Cons, MN_SolventRatio, MN_FeedCap, MN_OperPcnt, EIIMNFraction, HN_H2Cons, HN_SolventRatio, HN_FeedCap, HN_OperPcnt, EIIHNFraction, BRS_H2Cons, BRS_SolventRatio, BRS_FeedCap, BRS_OperPcnt, EIIBRSFraction, EIIRefWaxPcnt, EIIMicroWaxPcnt, FeedDensity, BtmProdPcnt, FZTemp, FZPress, EIIVGODesulf, EIIDieselDesulf, EIIVGOtoLt, EIIDieselToLt, H2Cons, SolventRatio, SumTempDiff, Yield, TotalFeed, VGOFeed
FROM ProfileFuels.dbo.DefUnitFactorData WHERE RefineryID = @RefineryID
/*
INSERT LoadEDCStabilizers (RefineryID, DataSet, EffDate, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB)
SELECT RefineryID, DataSet, EffDate, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB
FROM ProfileFuels.dbo.LoadEDCStabilizers WHERE RefineryID = @RefineryID AND DataSet = @Dataset
*/
INSERT LoadedEDCStabilizers (RefineryID, DataSet, EffDate, EffUntil, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB)
SELECT RefineryID, DataSet, EffDate, EffUntil, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB
FROM ProfileFuels.dbo.LoadedEDCStabilizers WHERE RefineryID = @RefineryID AND DataSet = @Dataset
/*
INSERT LoadRoutHist (RefineryID, DataSet, PeriodStart, RoutCostLocal, RoutMatlLocal)
SELECT RefineryID, DataSet, PeriodStart, RoutCostLocal, RoutMatlLocal
FROM ProfileFuels.dbo.LoadRoutHist WHERE RefineryID = @RefineryID AND DataSet = @Dataset
*/
INSERT MaintRoutHist (RefineryID, DataSet, PeriodStart, PeriodEnd, Currency, RoutCost, RoutMatl, Reported)
SELECT RefineryID, DataSet, PeriodStart, PeriodEnd, Currency, RoutCost, RoutMatl, Reported
FROM ProfileFuels.dbo.MaintRoutHist WHERE RefineryID = @RefineryID AND DataSet = @Dataset

INSERT MaintTA (RefineryID, DataSet, UnitID, TAID, SchedTAInt, TADate, TAHrsDown, TACostLocal, TACostUS, TAMatlLocal, TAMatlUS, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS, PrevTADate, TAExceptions, TACurrency, TAIntDays, TAIntYrs, AnnTACostLocal, AnnTAMatlLocal, AnnTACost, AnnTAMatl, AvgTANum, AnnTADownDays, TAOCCWHr, TAMPSOVT, TAMPSWHr, TAEffort, TAOCCMPSRatio, TACompWHr, TAContWHr, TAContEmpRatio, TAEmpOVTPcnt, AnnTAOCCSTH, AnnTAOCCOVT, AnnTACompOCCWHr, AnnTAMPSSTH, AnnTAMPSOVT, AnnTACompMPSWHr, AnnTAContOCCWHr, AnnTAContMPSWHr, AnnTAOCCWHr, AnnTAMPSWHr, AnnTAWHr, MechUnavailTA, TAMatlPcnt, RestartDate, ProcessID, NextTADate)
SELECT RefineryID, DataSet, UnitID, TAID, SchedTAInt, TADate, TAHrsDown, TACostLocal, TACostUS, TAMatlLocal, TAMatlUS, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS, PrevTADate, TAExceptions, TACurrency, TAIntDays, TAIntYrs, AnnTACostLocal, AnnTAMatlLocal, AnnTACost, AnnTAMatl, AvgTANum, AnnTADownDays, TAOCCWHr, TAMPSOVT, TAMPSWHr, TAEffort, TAOCCMPSRatio, TACompWHr, TAContWHr, TAContEmpRatio, TAEmpOVTPcnt, AnnTAOCCSTH, AnnTAOCCOVT, AnnTACompOCCWHr, AnnTAMPSSTH, AnnTAMPSOVT, AnnTACompMPSWHr, AnnTAContOCCWHr, AnnTAContMPSWHr, AnnTAOCCWHr, AnnTAMPSWHr, AnnTAWHr, MechUnavailTA, TAMatlPcnt, RestartDate, ProcessID, NextTADate
FROM ProfileFuels.dbo.MaintTA WHERE RefineryID = @RefineryID AND DataSet = @Dataset

INSERT MaintTACost (RefineryID, DataSet, UnitID, TAID, Currency, TACost, TAMatl, AnnTACost, AnnTAMatl, TADate, NextTADate, ProcessID, RestartDate)
SELECT RefineryID, DataSet, UnitID, TAID, Currency, TACost, TAMatl, AnnTACost, AnnTAMatl, TADate, NextTADate, ProcessID, RestartDate
FROM ProfileFuels.dbo.MaintTACost WHERE RefineryID = @RefineryID AND DataSet = @Dataset

INSERT MaterialPrices (RefineryID, StudyYear, MaterialID, PricePerBbl)
SELECT RefineryID, StudyYear, MaterialID, PricePerBbl
FROM ProfileFuels.dbo.MaterialPrices WHERE RefineryID = @RefineryID

INSERT ReadyForCalcs (RefineryID, DataSet, CalcsStarted, Uploading, LastUpdate)
SELECT RefineryID, DataSet, CalcsStarted, Uploading, LastUpdate
FROM ProfileFuels.dbo.ReadyForCalcs WHERE RefineryID = @RefineryID AND DataSet = @Dataset

INSERT RefineryFactors (RefineryID, EffDate, EffUntil, RVLocFactor, InflFactor, AvgInputBPD, CrudeDest, FlareLoss, ClientCEF_PUR_NG, ClientCEF_PUR_FG, ClientCEF_PUR_LBG, ClientCEF_PUR_HPF, ClientCEF_PRO_FG, ClientCEF_PRO_FCG, ClientCEF_PRO_FXG, ClientCEF_PRO_PSG, ClientCEF_PRO_PXG, ClientCEF_PRO_LBG, ClientCEF_TXI_NG, ClientCEF_TXI_FG, ClientCEF_TXI_LBG, ClientCEF_TXO_NG, ClientCEF_TXO_FG, ClientCEF_TXO_LBG, ClientCEF_SOL_NG, ClientCEF_SOL_FG, ClientCEF_SOL_LBG)
SELECT RefineryID, EffDate, EffUntil, RVLocFactor, InflFactor, AvgInputBPD, CrudeDest, FlareLoss, ClientCEF_PUR_NG, ClientCEF_PUR_FG, ClientCEF_PUR_LBG, ClientCEF_PUR_HPF, ClientCEF_PRO_FG, ClientCEF_PRO_FCG, ClientCEF_PRO_FXG, ClientCEF_PRO_PSG, ClientCEF_PRO_PXG, ClientCEF_PRO_LBG, ClientCEF_TXI_NG, ClientCEF_TXI_FG, ClientCEF_TXI_LBG, ClientCEF_TXO_NG, ClientCEF_TXO_FG, ClientCEF_TXO_LBG, ClientCEF_SOL_NG, ClientCEF_SOL_FG, ClientCEF_SOL_LBG
FROM ProfileFuels.dbo.RefineryFactors WHERE RefineryID = @RefineryID

INSERT RefineryInfo (RefineryID, Company, Location, CoordName, CoordTitle, CoordAddr1, CoordAddr2, CoordAddr3, CoordPhone, CoordFax, CoordEMail)
SELECT RefineryID, Company, Location, CoordName, CoordTitle, CoordAddr1, CoordAddr2, CoordAddr3, CoordPhone, CoordFax, CoordEMail
FROM ProfileFuels.dbo.RefineryInfo WHERE RefineryID = @RefineryID

INSERT StudyT2_FCC (RefineryID, EffDate, EffUntil, UnitID, FeedTemp, PHeatTemp, Conv, CatOilRatio, Coke, FeedGravity, FeedDensity, FeedSulfur, FeedConCarbon, FeedASTM10, FeedASTM50, FeedASTM90, FeedAniline, UOPK, EstCokeSulfur, FeedUVirginGO, FeedUCrackedGO, FeedUAGO, FeedUVGO, FeedUDAO, FeedUCGO, FeedUARC, FeedUVR, FeedULube, FeedHVirginGO, FeedHCrackedGO, FeedHAGO, FeedHVGO, FeedHDAO, FeedHCGO, FeedHARC, FeedHVR, FeedHLube, FeedN2, FeedMetals, FeedUVGOGravity, FeedUVGODensity, FeedUVGOSulfur, FeedUVGOAniline, FeedUVGOUOPK, FeedUVRGravity, FeedUVRDensity, FeedUVRSulfur, FeedUVRConCarbon, FeedHVGOGravity, FeedHVGODensity, FeedHVGOSulfur, FeedHVGOAniline, FeedHVGOUOPK, FeedHVRGravity, FeedHVRDensity, FeedHVRSulfur, FeedHVRConCarbon, FeedMaxSulfur, FeedMaxN2, ProdC2, ProdC3, ProdC4, ProdC3C4, ProdC4Lt, ProdGasNap, ProdLCO, ProdHCO, ProdDSO, Yield, ProdSulfur, CokePcnt, TotYield, CSubA, UOP_KFactor, FeedUVGOUCap, FeedUVRUCap, FeedHVGOUCap, FeedHVRUCap, CatGasoUCap, UtilCapMT, FeedUVGOUCapMT, FeedUVRUCapMT, FeedHVGOUCapMT, FeedHVRUCapMT, FeedUOthUCap, FeedHOthUCap, FeedOthUCap, EstFeedUOthUCapMT, EstFeedHOthUCapMT, EstFeedOthUCapMT, EstFeedOthDensity, EstFeedOthSulfur, COBlrFiredCap)
SELECT RefineryID, EffDate, EffUntil, UnitID, FeedTemp, PHeatTemp, Conv, CatOilRatio, Coke, FeedGravity, FeedDensity, FeedSulfur, FeedConCarbon, FeedASTM10, FeedASTM50, FeedASTM90, FeedAniline, UOPK, EstCokeSulfur, FeedUVirginGO, FeedUCrackedGO, FeedUAGO, FeedUVGO, FeedUDAO, FeedUCGO, FeedUARC, FeedUVR, FeedULube, FeedHVirginGO, FeedHCrackedGO, FeedHAGO, FeedHVGO, FeedHDAO, FeedHCGO, FeedHARC, FeedHVR, FeedHLube, FeedN2, FeedMetals, FeedUVGOGravity, FeedUVGODensity, FeedUVGOSulfur, FeedUVGOAniline, FeedUVGOUOPK, FeedUVRGravity, FeedUVRDensity, FeedUVRSulfur, FeedUVRConCarbon, FeedHVGOGravity, FeedHVGODensity, FeedHVGOSulfur, FeedHVGOAniline, FeedHVGOUOPK, FeedHVRGravity, FeedHVRDensity, FeedHVRSulfur, FeedHVRConCarbon, FeedMaxSulfur, FeedMaxN2, ProdC2, ProdC3, ProdC4, ProdC3C4, ProdC4Lt, ProdGasNap, ProdLCO, ProdHCO, ProdDSO, Yield, ProdSulfur, CokePcnt, TotYield, CSubA, UOP_KFactor, FeedUVGOUCap, FeedUVRUCap, FeedHVGOUCap, FeedHVRUCap, CatGasoUCap, UtilCapMT, FeedUVGOUCapMT, FeedUVRUCapMT, FeedHVGOUCapMT, FeedHVRUCapMT, FeedUOthUCap, FeedHOthUCap, FeedOthUCap, EstFeedUOthUCapMT, EstFeedHOthUCapMT, EstFeedOthUCapMT, EstFeedOthDensity, EstFeedOthSulfur, COBlrFiredCap
FROM ProfileFuels.dbo.StudyT2_FCC WHERE RefineryID = @RefineryID

INSERT StudyT2_H2PURE (RefineryID, EffDate, EffUntil, UnitID, FeedH2, H2Loss)
SELECT RefineryID, EffDate, EffUntil, UnitID, FeedH2, H2Loss
FROM ProfileFuels.dbo.StudyT2_H2PURE WHERE RefineryID = @RefineryID

INSERT StudyT2_HYG (RefineryID, EffDate, EffUntil, UnitID, FeedH2, FeedC1, FeedC2, FeedC3, FeedC4, FeedCO, FeedCO2, FeedN2, FeedOth, FeedLPG, FeedNap, FeedKero, FeedDiesel, FeedHGO, FeedARC, FeedVacResid, FeedLubeExtracts, FeedAsp, FeedCoke, CO2RejectType, H2Loss)
SELECT RefineryID, EffDate, EffUntil, UnitID, FeedH2, FeedC1, FeedC2, FeedC3, FeedC4, FeedCO, FeedCO2, FeedN2, FeedOth, FeedLPG, FeedNap, FeedKero, FeedDiesel, FeedHGO, FeedARC, FeedVacResid, FeedLubeExtracts, FeedAsp, FeedCoke, CO2RejectType, H2Loss
FROM ProfileFuels.dbo.StudyT2_HYG WHERE RefineryID = @RefineryID

INSERT StudyValues (RefineryID, EffDate, EffUntil, Property, StudyValue, StudyP1Value)
SELECT RefineryID, EffDate, EffUntil, Property, StudyValue, StudyP1Value
FROM ProfileFuels.dbo.StudyValues WHERE RefineryID = @RefineryID

INSERT UnitDefaults (RefineryID, UnitID, EffDate, EffUntil, Ratio)
SELECT RefineryID, UnitID, EffDate, EffUntil, Ratio
FROM ProfileFuels.dbo.UnitDefaults WHERE RefineryID = @RefineryID

INSERT UnitList (RefineryID, UnitID, TAID)
SELECT RefineryID, UnitID, TAID
FROM ProfileFuels.dbo.UnitList WHERE RefineryID = @RefineryID

INSERT StudyTankage (RefineryID, DataSet, EffDate, EffUntil, TankType, FuelsStorage, NumTank, AvgLevel)
SELECT RefineryID, DataSet, EffDate, EffUntil, TankType, FuelsStorage, NumTank, AvgLevel
FROM ProfileFuels.dbo.StudyTankage WHERE RefineryID = @RefineryID AND DataSet = @Dataset

DECLARE @PeriodYear smallint, @PeriodMonth tinyint, @SubmissionID int, @RptCurrency varchar(5), @RptCurrencyT15 varchar(5), @UOM varchar(5), @Company nvarchar(30), @Location nvarchar(30)
DECLARE @CoordName nvarchar(50), @CoordTitle nvarchar(50), @CoordPhone varchar(40), @CoordEMail varchar(255), @BridgeVersion varchar(10), @ClientVersion varchar(10), @Submitted datetime
DECLARE @OrigSubmissionID int

DECLARE cSubs CURSOR LOCAL FAST_FORWARD FOR
	SELECT PeriodYear, PeriodMonth, SubmissionID, RptCurrency, RptCurrencyT15, UOM, Company, Location, CoordName, CoordTitle, CoordPhone, CoordEMail, BridgeVersion, ClientVersion, Submitted
	FROM ProfileFuels.dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @Dataset AND PeriodStart >= ISNULL(@EarliestDate, '1/1/1980')
OPEN cSubs
WHILE 1=1
BEGIN
	FETCH NEXT FROM cSubs INTO @PeriodYear, @PeriodMonth, @OrigSubmissionID, @RptCurrency, @RptCurrencyT15, @UOM, @Company, @Location, @CoordName, @CoordTitle, @CoordPhone, @CoordEMail, @BridgeVersion, @ClientVersion, @Submitted
	IF @@FETCH_STATUS <> 0
		BREAK

	EXEC spSubmissionID @RefineryID, @Dataset, @PeriodYear, @PeriodMonth
	SELECT @SubmissionID = SubmissionID FROM Submissions WHERE RefineryID = @RefineryID AND Dataset = @Dataset AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND UseSubmission = 1

	EXEC spPrepareForUpload @SubmissionID
	
	UPDATE Submissions 
	SET RptCurrency = @RptCurrency, RptCurrencyT15 = @RptCurrencyT15, UOM = @UOM, Company = @Company, Location = @Location, CoordName = @CoordName, CoordTitle = @CoordTitle, CoordPhone = @CoordPhone, CoordEMail = @CoordEMail, BridgeVersion = @BridgeVersion, ClientVersion = @ClientVersion, Submitted = @Submitted
	WHERE SubmissionID = @SubmissionID

	INSERT Config(SubmissionID, UnitID, SortKey, ProcessID, UnitName, ProcessType, RptCap, RptCapUOM, UtilPcnt, RptStmCap, RptStmCapUOM, StmUtilPcnt, DesignFeedSulfur, EnergyPcnt)
	SELECT @SubmissionID, UnitID, SortKey, ProcessID, UnitName, ProcessType, RptCap, RptCapUOM, UtilPcnt, RptStmCap, RptStmCapUOM, StmUtilPcnt, DesignFeedSulfur, EnergyPcnt
	FROM ProfileFuels.dbo.Config WHERE SubmissionID = @OrigSubmissionID
	
	INSERT ConfigRS(SubmissionID, UnitID, ProcessID, ProcessType, AvgSize, Throughput, Vessels, PcntOwnership)
	SELECT @SubmissionID, UnitID, ProcessID, ProcessType, AvgSize, Throughput, Vessels, PcntOwnership
	FROM ProfileFuels.dbo.ConfigRS WHERE SubmissionID = @OrigSubmissionID
	
	INSERT Inventory(SubmissionID, TankType, SortKey, TotStorage, MktgStorage, MandStorage, FuelsStorage, NumTank, LeasedPcnt, AvgLevel, MandLevel, Inven)
	SELECT @SubmissionID, TankType, SortKey, TotStorage, MktgStorage, MandStorage, FuelsStorage, NumTank, LeasedPcnt, AvgLevel, MandLevel, Inven
	FROM ProfileFuels.dbo.Inventory WHERE SubmissionID = @OrigSubmissionID

	INSERT ProcessData (SubmissionID, UnitID, Property, RptValue, RptUOM)
	SELECT @SubmissionID, UnitID, Property, RptValue, RptUOM
	FROM ProfileFuels.dbo.ProcessData WHERE SubmissionID = @OrigSubmissionID

	INSERT Opex(SubmissionID,Currency,Scenario,DataType,OCCSal,MPSSal,OCCBen,MPSBen,MaintMatl,ContMaintLabor,ContMaintMatl,OthCont,Equip,Tax,Insur,TAAdj,Envir,OthNonVol,GAPers,STNonVol,Antiknock,Chemicals,Catalysts,Royalties,PurElec,PurSteam,PurOth,PurFG,PurLiquid,PurSolid,RefProdFG,RefProdOth,EmissionsPurch,EmissionsCredits,EmissionsTaxes,OthVol,STVol,TotCashOpEx,GANonPers,InvenCarry,Depreciation,Interest,STNonCash,TotRefExp,Cogen,OthRevenue,ThirdPartyTerminalRM,ThirdPartyTerminalProd,POXO2,PMAA,FireSafetyLoss,EnvirFines,ExclOth,TotExpExcl,STSal,STBen,PersCostExclTA,PersCost,EnergyCost,NEOpex)
	SELECT @SubmissionID,Currency,Scenario,DataType,OCCSal,MPSSal,OCCBen,MPSBen,MaintMatl,ContMaintLabor,ContMaintMatl,OthCont,Equip,Tax,Insur,TAAdj,Envir,OthNonVol,GAPers,STNonVol,Antiknock,Chemicals,Catalysts,Royalties,PurElec,PurSteam,PurOth,PurFG,PurLiquid,PurSolid,RefProdFG,RefProdOth,EmissionsPurch,EmissionsCredits,EmissionsTaxes,OthVol,STVol,TotCashOpEx,GANonPers,InvenCarry,Depreciation,Interest,STNonCash,TotRefExp,Cogen,OthRevenue,ThirdPartyTerminalRM,ThirdPartyTerminalProd,POXO2,PMAA,FireSafetyLoss,EnvirFines,ExclOth,TotExpExcl,STSal,STBen,PersCostExclTA,PersCost,EnergyCost,NEOpex
	FROM ProfileFuels.dbo.OpEx WHERE SubmissionID = @OrigSubmissionID AND DataType = 'RPT'

	INSERT OpExAdd(SubmissionID,Currency,DataType,OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock,OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,OCCBen,MPSBenAbs,MPSBenInsur,MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth,MPSBen,MaintMatl,ContMaintMatl,ContMaintLabor,ContMaintInspect,OthContProcOp,OthContTransOp,OthContFire,OthContVacTrucks,OthContConsult,OthContInsp,OthContSecurity,OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin,OthContLegal,OthContOth,OthCont,EquipMaint,EquipNonMaint,Equip,Tax,Insur,InsurBI,InsurPC,InsurOth,EnvirDisp,EnvirPermits,EnvirFines,EnvirSpill,EnvirLab,EnvirEng,EnvirOth,Envir,OthNonVolSupply,OthNonVolSafety,OthNonVolNonPersSafety,OthNonVolComm,OthNonVolDonations,OthNonVolDues,OthNonVolNonContribPers,OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks,OthNonVolExpat,OthNonVolOth,OthNonVol,Royalties,ChemicalsAntiknock,ChemicalsAlkyAcid,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK,ChemicalsMEK,ChemicalsLube,ChemicalsH2OTreat,ChemicalsProcess,ChemicalsAcid,ChemicalsOthAcid,ChemicalsDewaxAids,ChemicalsGasAdd,ChemicalsDieselAdd,ChemicalsOthAdd,ChemicalsAdditives,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv,ChemicalsWasteH2O,ChemicalsOth,Chemicals,CatalystsFCC,CatalystsHYC,CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT,CatalystsHYT,CatalystsREF,CatalystsHYG,CatalystsS2Plant,CatalystsPetChem,CatalystsCDWAX,CatalystsHYFT,CatalystsOth,Catalysts,PurOthN2,PurOthH2O,PurOthOth,PurOth,OthVolDemCrude,OthVolDemLightering,OthVolDemProd,OthVolDemurrage,OthVolOth,OthVol,OthRevenue,EmissionsTaxes,EmissionsPurch,EmissionsCredits)
	SELECT @SubmissionID,Currency,DataType,OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock,OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,OCCBen,MPSBenAbs,MPSBenInsur,MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth,MPSBen,MaintMatl,ContMaintMatl,ContMaintLabor,ContMaintInspect,OthContProcOp,OthContTransOp,OthContFire,OthContVacTrucks,OthContConsult,OthContInsp,OthContSecurity,OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin,OthContLegal,OthContOth,OthCont,EquipMaint,EquipNonMaint,Equip,Tax,Insur,InsurBI,InsurPC,InsurOth,EnvirDisp,EnvirPermits,EnvirFines,EnvirSpill,EnvirLab,EnvirEng,EnvirOth,Envir,OthNonVolSupply,OthNonVolSafety,OthNonVolNonPersSafety,OthNonVolComm,OthNonVolDonations,OthNonVolDues,OthNonVolNonContribPers,OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks,OthNonVolExpat,OthNonVolOth,OthNonVol,Royalties,ChemicalsAntiknock,ChemicalsAlkyAcid,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK,ChemicalsMEK,ChemicalsLube,ChemicalsH2OTreat,ChemicalsProcess,ChemicalsAcid,ChemicalsOthAcid,ChemicalsDewaxAids,ChemicalsGasAdd,ChemicalsDieselAdd,ChemicalsOthAdd,ChemicalsAdditives,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv,ChemicalsWasteH2O,ChemicalsOth,Chemicals,CatalystsFCC,CatalystsHYC,CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT,CatalystsHYT,CatalystsREF,CatalystsHYG,CatalystsS2Plant,CatalystsPetChem,CatalystsCDWAX,CatalystsHYFT,CatalystsOth,Catalysts,PurOthN2,PurOthH2O,PurOthOth,PurOth,OthVolDemCrude,OthVolDemLightering,OthVolDemProd,OthVolDemurrage,OthVolOth,OthVol,OthRevenue,EmissionsTaxes,EmissionsPurch,EmissionsCredits
	FROM ProfileFuels.dbo.OpexAdd WHERE SubmissionID = @OrigSubmissionID AND DataType = 'RPT'

	INSERT OpExAll(SubmissionID,Currency,Scenario,DataType,OCCSal,MPSSal,OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock,OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,OCCBen,MPSBenAbs,MPSBenInsur,MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth,MPSBen,MaintMatl,ContMaintMatl,EquipMaint,MaintMatlST,ContMaintLabor,ContMaintInspect,ContMaintLaborST,OthContProcOp,OthContTransOp,OthContFire,OthContVacTrucks,OthContConsult,OthContSecurity,OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin,OthContLegal,OthContOth,OthCont,TAAdj,EnvirDisp,EnvirPermits,EnvirFines,EnvirSpill,EnvirLab,EnvirEng,EnvirOth,Envir,EquipNonMaint,Tax,InsurBI,InsurPC,InsurOth,OthNonVolSupply,OthNonVolSafety,OthNonVolNonPersSafety,OthNonVolComm,OthNonVolDonations,OthNonVolNonContribPers,OthNonVolDues,OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks,OthNonVolExtraExpat,OthNonVolOth,OthNonVol,STNonVol,GAPers,ChemicalsAntiknock,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat,ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd,ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv,ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK,ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv,ChemicalsAcid,ChemicalsDewaxAids,ChemicalsOth,Chemicals,CatalystsFCC,CatalystsHYC,CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT,CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG,CatalystsS2Plant,CatalystsPetChem,CatalystsOth,Catalysts,Royalties,PurElec,PurSteam,PurFG,PurLiquid,PurSolid,PurOthN2,PurOthH2O,PurOthOth,PurOth,RefProdFG,RefProdOth,EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth,OthVol,GANonPers,InvenCarry,Depreciation,Interest,STNonCash,TotRefExp,Cogen,OthRevenue,ThirdPartyTerminalRM,ThirdPartyTerminalProd,POXO2,PMAA,OthVolDemCrude,OthVolDemLightering,OthVolDemProd,STVol,TotCashOpEx,ExclFireSafety,ExclEnvirFines,ExclOth,TotExpExcl,STSal,STBen,PersCostExclTA,PersCost,EnergyCost,NEOpex,CatalystsFCCAdditives,CatalystsFCCECat,CatalystsFCCECatSales,CatalystsFCCFresh,EnvirMonitor)
	SELECT @SubmissionID,Currency,Scenario,DataType,OCCSal,MPSSal,OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock,OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,OCCBen,MPSBenAbs,MPSBenInsur,MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth,MPSBen,MaintMatl,ContMaintMatl,EquipMaint,MaintMatlST,ContMaintLabor,ContMaintInspect,ContMaintLaborST,OthContProcOp,OthContTransOp,OthContFire,OthContVacTrucks,OthContConsult,OthContSecurity,OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin,OthContLegal,OthContOth,OthCont,TAAdj,EnvirDisp,EnvirPermits,EnvirFines,EnvirSpill,EnvirLab,EnvirEng,EnvirOth,Envir,EquipNonMaint,Tax,InsurBI,InsurPC,InsurOth,OthNonVolSupply,OthNonVolSafety,OthNonVolNonPersSafety,OthNonVolComm,OthNonVolDonations,OthNonVolNonContribPers,OthNonVolDues,OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks,OthNonVolExtraExpat,OthNonVolOth,OthNonVol,STNonVol,GAPers,ChemicalsAntiknock,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat,ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd,ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv,ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK,ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv,ChemicalsAcid,ChemicalsDewaxAids,ChemicalsOth,Chemicals,CatalystsFCC,CatalystsHYC,CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT,CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG,CatalystsS2Plant,CatalystsPetChem,CatalystsOth,Catalysts,Royalties,PurElec,PurSteam,PurFG,PurLiquid,PurSolid,PurOthN2,PurOthH2O,PurOthOth,PurOth,RefProdFG,RefProdOth,EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth,OthVol,GANonPers,InvenCarry,Depreciation,Interest,STNonCash,TotRefExp,Cogen,OthRevenue,ThirdPartyTerminalRM,ThirdPartyTerminalProd,POXO2,PMAA,OthVolDemCrude,OthVolDemLightering,OthVolDemProd,STVol,TotCashOpEx,ExclFireSafety,ExclEnvirFines,ExclOth,TotExpExcl,STSal,STBen,PersCostExclTA,PersCost,EnergyCost,NEOpex,CatalystsFCCAdditives,CatalystsFCCECat,CatalystsFCCECatSales,CatalystsFCCFresh,EnvirMonitor
	FROM ProfileFuels.dbo.OpExAll WHERE SubmissionID = @OrigSubmissionID AND DataType = 'RPT'
	
	INSERT Pers(SubmissionID, PersID, SortKey, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, MaintPcnt)
	SELECT @SubmissionID, PersID, SortKey, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, MaintPcnt
	FROM ProfileFuels.dbo.Pers WHERE SubmissionID = @OrigSubmissionID AND PersID NOT LIKE '%TAADJ'

	INSERT Absence (SubmissionID, SortKey, CategoryID, OCCAbs, MPSAbs, TotAbs)
	SELECT @SubmissionID, SortKey, CategoryID, OCCAbs, MPSAbs, TotAbs
	FROM ProfileFuels.dbo.Absence WHERE SubmissionID = @OrigSubmissionID
		
	INSERT Crude (SubmissionID, CrudeID, CNum, CrudeName, BBL, Gravity, Sulfur, CostPerBBL)
	SELECT @SubmissionID, CrudeID, CNum, CrudeName, BBL, Gravity, Sulfur, CostPerBBL
	FROM ProfileFuels.dbo.Crude WHERE SubmissionID = @OrigSubmissionID

	INSERT Yield (SubmissionID, SortKey, Category, MaterialID, MaterialName, BBL, Gravity, Density, MT, PriceLocal)
	SELECT @SubmissionID, SortKey, Category, MaterialID, MaterialName, BBL, Gravity, Density, MT, PriceLocal
	FROM ProfileFuels.dbo.Yield WHERE SubmissionID = @OrigSubmissionID
	
	INSERT Energy (SubmissionID, TransCode, EnergyType, TransType, TransferTo, RptSource, RptPriceLocal, Hydrogen, Methane, Ethane, Ethylene, Propane, Propylene, Butane, Isobutane, Butylenes, C5Plus, CO, CO2, N2, Inerts)
	SELECT @SubmissionID, TransCode, EnergyType, TransType, TransferTo, RptSource, RptPriceLocal, Hydrogen, Methane, Ethane, Ethylene, Propane, Propylene, Butane, Isobutane, Butylenes, C5Plus, CO, CO2, N2, Inerts
	FROM ProfileFuels.dbo.Energy WHERE SubmissionID = @OrigSubmissionID

	INSERT Electric (SubmissionID, TransCode, EnergyType, TransType, TransferTo, RptMWH, PriceLocal, RptGenEff)
	SELECT @SubmissionID, TransCode, EnergyType, TransType, TransferTo, RptMWH, PriceLocal, RptGenEff
	FROM ProfileFuels.dbo.Electric WHERE SubmissionID = @OrigSubmissionID

	INSERT MaintRout (SubmissionID, UnitID, ProcessID, RegNum, RegDown, RegSlow, MaintNum, MaintDown, MaintSlow, OthNum, OthDown, OthSlow, RoutCostLocal, RoutMatlLocal, OthDownEconomic, OthDownExternal, OthDownUnitUpsets, OthDownOffsiteUpsets, OthDownOther, UnpRegNum, UnpRegDown, UnpMaintNum, UnpMaintDown, UnpOthNum, UnpOthDown, UnpDown)
	SELECT @SubmissionID, UnitID, ProcessID, RegNum, RegDown, RegSlow, MaintNum, MaintDown, MaintSlow, OthNum, OthDown, OthSlow, RoutCostLocal, RoutMatlLocal, OthDownEconomic, OthDownExternal, OthDownUnitUpsets, OthDownOffsiteUpsets, OthDownOther, UnpRegNum, UnpRegDown, UnpMaintNum, UnpMaintDown, UnpOthNum, UnpOthDown, UnpDown
	FROM ProfileFuels.dbo.MaintRout WHERE SubmissionID = @OrigSubmissionID
	
	INSERT EDCStabilizers (SubmissionID, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB)
	SELECT @SubmissionID, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB
	FROM ProfileFuels.dbo.EDCStabilizers WHERE SubmissionID = @OrigSubmissionID
/*
	INSERT LoadTA(SubmissionID, UnitID, TAID, SchedTAInt, TADate, TAHrsDown, TACostLocal, TAMatlLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS, PrevTADate, TAExceptions, TACurrency, ProcessID)
	SELECT @SubmissionID, UnitID, TAID, SchedTAInt, TADate, TAHrsDown, TACostLocal, TAMatlLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TAMPSOVTPcnt, TAContOCC, TAContMPS, PrevTADate, TAExceptions, TACurrency, ProcessID
	FROM ProfileFuels.dbo.LoadTA WHERE RefineryID = @RefineryID AND (TADate < dbo.BuildDate(@PeriodYear, @PeriodMonth, 28) OR TADate IS NULL)
*/
	INSERT RefTargets (SubmissionID, Property, Target, CurrencyCode)
	SELECT @SubmissionID, Property, Target, CurrencyCode
	FROM ProfileFuels.dbo.RefTargets WHERE SubmissionID = @OrigSubmissionID

	INSERT UnitTargets (SubmissionID, UnitID, MechAvail, OpAvail, OnStream, UtilPcnt, RoutCost, TACost, CurrencyCode, UnitEII)
	SELECT @SubmissionID, UnitID, MechAvail, OpAvail, OnStream, UtilPcnt, RoutCost, TACost, CurrencyCode, UnitEII
	FROM ProfileFuels.dbo.UnitTargets WHERE SubmissionID = @OrigSubmissionID
	
	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT @SubmissionID, UnitID, Property, Target, CurrencyCode
	FROM ProfileFuels.dbo.UnitTargetsNew WHERE SubmissionID = @OrigSubmissionID

	INSERT UserDefined (SubmissionID, HeaderText, VariableDesc, RptValue, DecPlaces, RptValue_Target, RptValue_Avg, RptValue_YTD)
	SELECT @SubmissionID, HeaderText, VariableDesc, RptValue, DecPlaces, RptValue_Target, RptValue_Avg, RptValue_YTD
	FROM ProfileFuels.dbo.UserDefined WHERE SubmissionID = @OrigSubmissionID
	
	EXEC spCompleteSubmission @SubmissionID, 1

END
CLOSE cSubs
DEALLOCATE cSubs

UPDATE ReadyForCalcs 
SET Uploading = 0, LastUpdate = GetDate()
WHERE RefineryID = @RefineryID AND DataSet = @Dataset

RAISERROR (50001, 7, 1)

