﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************

Description:      
Create a Reflist for Company and add Trends And ReportGroups

Taken from Mike Browns 

Author: Rodrick Blanton
Date: 13 Dec 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/
CREATE PROCEDURE dbo.spCreateReflist 
	-- Add the parameters for the stored procedure here
	@CompanyID VARCHAR(50)
	, @StudyYear INT
	, @YearsInTrend INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ListName VARCHAR(50)
	, @RefListNo INT
	, @StudyName VARCHAR(50) = 'Power'
	, @TwoDigitYear VARCHAR(2)
	, @ReportRefnum VARCHAR(50)
	, @Batch VARCHAR(50)
	, @TrendStartYear INT

    BEGIN TRY

		-- SETUP

		SELECT @TwoDigitYear = CONVERT(VARCHAR(2), RIGHT(@StudyYear,2))
		, @TrendStartYear = @StudyYear - @YearsInTrend

		SELECT @ListName = @StudyName + @TwoDigitYear + ':' + @CompanyID

		DECLARE @DynamicSQL TABLE
		(
			dsID			UNIQUEIDENTIFIER DEFAULT(NEWID())
			, sqlStatement	NVARCHAR(MAX)
		)

		DECLARE @DynamicSQLCatch TABLE
		(
			dsID			UNIQUEIDENTIFIER
			, sqlStatement	NVARCHAR(MAX)
		)


		--INSERT INTO REFLIST LOOKUP TABLE

		INSERT INTO Reflist_LU(ListName, OWNER, DESCRIPTION)
		SELECT @listname, 'ADMIN', 'Company List'

		--GET NEW REFLISTNO

		SELECT @Reflistno = Max(reflistno) 
		FROM Reflist_Lu

		--INSERT INTO REFLIST LOOKUP TABLE

		INSERT INTO RefList
		SELECT @Reflistno, Refnum, StudyYear
		FROM Tsort 
		WHERE StudyYear >= @TrendStartYear
		AND Companyid = @Companyid 
		AND refnum  LIKE '%' + @TwoDigitYear
		ORDER BY REFNUM DESC, StudyYear DESC

		--INSERT INTO REPORT TABLE BY TREND YEAR

		INSERT INTO Report.ReportGroups(
			Refnum
			, ReportTitle
			, Listname
			, StudyYear
			, PostProcess
			, Batch
			, Keepupdated
		)
		SELECT DISTINCT  @StudyName + @TwoDigitYear + ':' + @CompanyID + RIGHT(StudyYear, 2)
		, @StudyName + @TwoDigitYear + ':' + @CompanyID + RIGHT(StudyYear, 2)
		, @ListName
		, StudyYear
		, 'N'
		, @Batch
		, 'Y'
		FROM Tsort 
		WHERE StudyYear >= @TrendStartYear
		AND CompanyID = @CompanyID 
		AND refnum  LIKE '%' + RIGHT(StudyYear, 2)

		INSERT @DynamicSQL (sqlStatement)
		SELECT DISTINCT 'EXEC Report.UpdateReportGroup ''' + @StudyName + @TwoDigitYear + ':' + @Companyid + RIGHT(StudyYear, 2) + '''' 
		FROM Tsort 
		WHERE StudyYear >= @TrendStartYear
		AND Companyid = @Companyid 
		AND refnum  LIKE '%' + RIGHT(StudyYear, 2)

		WHILE
		(
			SELECT COUNT(*)
			FROM @DynamicSQL a
			WHERE a.dsID NOT IN (
									SELECT b.dsID 
									FROM @DynamicSQLCatch b
								)
		) > 0
		BEGIN
			
			DECLARE @stmt NVARCHAR(MAX)
			, @dsID UNIQUEIDENTIFIER

			SELECT TOP 1 @dsID = dsID, @stmt = sqlStatement
			FROM @DynamicSQL a
			WHERE a.dsID NOT IN (
									SELECT b.dsID 
									FROM @DynamicSQLCatch b
								)

			EXECUTE sp_executesql @stmt

			INSERT @DynamicSQLCatch
			SELECT @dsID, @stmt

		END
	

	END TRY
	BEGIN CATCH

		SELECT  
		ERROR_NUMBER() AS ErrorNumber  
		,ERROR_SEVERITY() AS ErrorSeverity  
		,ERROR_STATE() AS ErrorState  
		,ERROR_PROCEDURE() AS ErrorProcedure  
		,ERROR_LINE() AS ErrorLine  
		,ERROR_MESSAGE() AS ErrorMessage;  

	END CATCH

END
GO
