﻿




DECLARE @CRVGroup VARCHAR(10) = 'SC'
--, @Variable VARCHAR(50) = 'PeakUnavail_Unp'
--, @Variable VARCHAR(50) = 'HeatRate'
--, @Variable VARCHAR(50) = 'TotCashLessFuelEmMWH'
--, @Variable VARCHAR(50) = 'TotCashLessFuelEmMW'
--, @Variable VARCHAR(50) = 'ThermEff'
--, @Variable VARCHAR(50) = 'InternalLessScrubberPcnt'
--, @Variable VARCHAR(50) = 'CUTI_Unp'
--, @Variable VARCHAR(50) = 'CUTI_P'
--, @Variable VARCHAR(50) = 'AnnOHCostMW'
--, @Variable VARCHAR(50) = 'AnnNonOHCostMWH'
--, @Variable VARCHAR(50) = 'AnnMaintCostMWH'
--, @Variable VARCHAR(50) = 'AnnLTSACostMWH'
, @SlideTemplateID INT = 149
, @SlideID INT
, @SlideChartID INT
, @SlideDataID INT




INSERT Slides
Select 'QDist' + @CRVGroup + REPLACE(@Variable, ' ','')
, 'Quartile Distribution'
, SubTitle
, Multi
, Sub
, SortOrder
, 1
, NULL
, NULL
From Slides 
where SlideID = @SlideTemplateID



SELECT @SlideID = SlideID
FROM Slides
WHERE Name = 'QDist' + @CRVGroup + REPLACE(@Variable, ' ','')

--SELECT @SlideID

IF ISNULL(@SlideID,0)>0
BEGIN
	INSERT SlideCharts
	(
		SlideID
		, ChartType
		, ChartYAxisTitle
		, XAxisMax
		, XAxisMajorUnit
	)
	Select @SlideID
		, 'Line'
		, @Variable + ' ' + @CRVGroup
		, 100
		, 25



	SELECT @SlideChartID = SlideChartID
	FROM SlideCharts
	WHERE SlideID = @SlideID


	--Select @SlideChartID


	IF ISNULL(@SlideChartID,0)>0
	BEGIN
		INSERT SlideData
		(
			SlideChartID
			, AxisNumber
			, AxisTitle
			, DataSource
			, Filter1
			, SlideColorGroup
			, Filter2
			, Filter3
			, DataType
			, Filter4
			, Filter5
			, BarGapWidth
			, LineFormat
			, Filter6
			, Filter7
			, Multi
		)
		SELECT
			@SlideChartID
			, 0
			, NULL
			, 'QuartileDist'
			, '@Option1'
			, 12
			, '@Option3'
			, @CRVGroup
			, 'ScatLine'
			, @Variable
			, '@Option2'
			, NULL
			, 5
			, '@CurrencyCode'
			, '@Metric'
			, NULL

		INSERT SlideData
		(
			SlideChartID
			, AxisNumber
			, AxisTitle
			, DataSource
			, Filter1
			, SlideColorGroup
			, Filter2
			, Filter3
			, DataType
			, Filter4
			, Filter5
			, BarGapWidth
			, LineFormat
			, Filter6
			, Filter7
			, Multi
		)
		SELECT
			@SlideChartID
			, 0
			, NULL
			, 'QuartileDistPoints'
			, '@Option1'
			, 12
			, '@Option3'
			, @CRVGRoup
			, 'DistPoints'
			, @Variable
			, '@Option2'
			, NULL
			, NULL
			, '@CurrencyCode'
			, '@Metric'
			, NULL



		Select s.SlideID, s.Name, s.Title, s.SubTitle, s.Active, sd.*
		From SlideData sd
		INNER JOIN SlideCharts sc ON sd.SlideChartID = sc.SlideChartID
		INNER JOIN Slides s ON sc.SlideID = s.SlideID
		Where sc.SlideChartID = @SlideChartID

	END
END







----------------------------------------------------------------------





Select s.SlideID, s.Name, s.Title, s.SubTitle, s.Active, sd.*
From SlideData sd
INNER JOIN SlideCharts sc ON sd.SlideChartID = sc.SlideChartID
INNER JOIN Slides s ON sc.SlideID = s.SlideID
Where sd.DataSource IN ('PerfSum')
--Where sd.DataSource IN ('GetDistList')
--Where sd.DataSource IN ('QuartileDist')
and Filter3 NOT IN ('LSC')
Order by  Filter3

Select s.SlideID, s.Name, s.Title, s.SubTitle, sc.ChartYAxisTitle, s.Active, sd.*
From SlideData sd
INNER JOIN SlideCharts sc ON sd.SlideChartID = sc.SlideChartID
INNER JOIN Slides s ON sc.SlideID = s.SlideID
Where s.Name like 'QDist%'


Select s.SlideID, s.Name, s.Title, s.SubTitle, s.Active, sd.*
From SlideData sd
INNER JOIN SlideCharts sc ON sd.SlideChartID = sc.SlideChartID
INNER JOIN Slides s ON sc.SlideID = s.SlideID
Where s.SlideID IN (104)


Select s.SlideID, s.Name, s.Title, s.SubTitle, s.Active, sc.* , sd.*
From SlideData sd
INNER JOIN SlideCharts sc ON sd.SlideChartID = sc.SlideChartID
INNER JOIN Slides s ON sc.SlideID = s.SlideID
Where sd.DataSource IN ('HeatRate')


--Update Slides
--Set Active = NULL
--Where SlideID in
--(
--Select s.SlideID
--From SlideData sd
--INNER JOIN SlideCharts sc ON sd.SlideChartID = sc.SlideChartID
--INNER JOIN Slides s ON sc.SlideID = s.SlideID
--Where sd.DataSource IN ('PerfSum')
--)



SELECT * FROM PerfSum('Power16: ENEL','2016','CG2','HeatRate','Power16','EUR',1)



Select * FROM SlideData

Select * From SlideCharts


Select * From CRVGroup


Select * From Slides where SlideID = 659



Select *
From Slides
Where Name = 'QDistLSCHeatRate' -- 659



Select *
From SlideCharts
Where SlideID = 660



--Update SlideCharts
--Set ChartYAxisTitle = 'PeakUnavail_Unp LSC'
--WHERE SlideChartID = 813


--Delete SlideCharts Where SlideChartID = 812


Select *
From SlideData
Where SlideDataID = 261



Select *
From SlideData
Where DataSource like 'QuartileDist%'


--Update SlideData
--Set SlideChartID = 813
--Where DataSource = 'QuartileDist'

--Update Slides
--Set Name = 'QDistLSCPeakUnavail_Unp'
--Where SlideID = 659


--Update SlideCharts
--Set ChartYAxisTitle = 'HeatRate LSC'
--Where SlideChartID = 814


--Delete From SlideData Where SlideDataID in (1625,1626)





--------------------------------------------------------------------------------------------------------------




/* Plots */
SELECT X = Percentile, y = Value, * FROM Ranking.RankView
WHERE ListName = 'Power16' AND RankBreak = 'CRVGroup' AND BreakValue = 'LSC' AND RankVariable = 'AnnMaintCostMWH'
ORDER BY Percentile

/* Quartiles */
SELECT * FROM Ranking.RankSummary
WHERE RefListName = 'Power16' AND RankBreak = 'CRVSizeGroup' AND BreakValue  ='CE1' AND RankVariable IN ('AnnMaintCostMWH', 'AnnNonOHCostMWH', 'AnnOHCostMWH', 'AnnLTSACostMWH', 'TotCashMW','TotCashMWH','TotCashLessFuelEmMW','TotCashLessFuelEmMWH')

