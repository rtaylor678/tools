﻿/***********************************************************************************
Description:      
Search for text within stored procedures
Script creates EXEC sp_helptext statements to allow you to see the SP details.
Copy the statments and run them individually.

Author: Rodrick Blanton

Date: Unknown - long time ago

Change History:     
01/23/2015 - rvb - Added comments section

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/ 
 
 
SELECT
   cmd = N'EXEC sp_helptext '''
       + QUOTENAME(OBJECT_SCHEMA_NAME([object_id]))
       + '.' + QUOTENAME([name]) + ''';'
FROM
   sys.procedures
WHERE
   OBJECT_DEFINITION([object_id]) LIKE N'%[INSERT_TEXT]%'