﻿/***********************************************************************************
Description:      
Column P Script for "Retube$ Trends-NA" worksheet in Olefin Reliability & Maintenance
3D Data Basis - NA Template.xlsm

Author: Rodrick Blanton
Date: 01 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		Jeanne doesn't want this query in a stored proc.  It's used in excel and
		she wants to be able to see it.  Values will remain hardcoded for now so
		they easily be modified by the user.
***********************************************************************************/



--SELECT DISTINCT
--       fur.refnum  
--       ,t.StudyYear
--       ,t.CoLoc
--       ,(SELECT SUM(InflAdjAnn_MaintRetubeTot_Cur) FROM Olefins.dbo.Furnaces WHERE Refnum = fur.Refnum) [InflAdjAnn_MaintRetubeTot_Cur]
--       ,t.Region
--       ,(SELECT SUM(ActCap) FROM Olefins.dbo.Furnaces WHERE Refnum = fur.Refnum) [ActCap]
--       --,InflAdjAnnRetubePerMT = (fur.InflAdjAnn_MaintRetubeTot_Cur)/(fur.actcap/1000)
--       --,Yr=LEFT(fur.refnum,4)
--       ,SUM(fur07.InflAdjAnn_MaintRetubeTot_Cur)/SUM((fur07.actcap/1000)) [2007]
--       ,SUM(fur09.InflAdjAnn_MaintRetubeTot_Cur)/SUM((fur09.actcap/1000)) [2009]
--       ,SUM(fur11.InflAdjAnn_MaintRetubeTot_Cur)/SUM((fur11.actcap/1000)) [2011]
--       ,SUM(fur13.InflAdjAnn_MaintRetubeTot_Cur)/SUM((fur13.actcap/1000)) [2013]
--       ,SUM(fur15.InflAdjAnn_MaintRetubeTot_Cur)/SUM((fur15.actcap/1000)) [2015]
--FROM Olefins.dbo.Furnaces fur
--FULL JOIN Olefins.dbo.TSort t ON fur.Refnum=t.Refnum
--INNER JOIN Olefins.dbo.GENSUM g   ON g.Refnum=t.refnum
--LEFT JOIN Olefins.dbo.Furnaces fur07 ON t.Refnum=fur07.Refnum AND t.StudyYear=2007
--LEFT JOIN Olefins.dbo.Furnaces fur09 ON t.Refnum=fur09.Refnum AND t.StudyYear=2009
--LEFT JOIN Olefins.dbo.Furnaces fur11 ON t.Refnum=fur11.Refnum AND t.StudyYear=2011
--LEFT JOIN Olefins.dbo.Furnaces fur13 ON t.Refnum=fur13.Refnum AND t.StudyYear=2013
--LEFT JOIN Olefins.dbo.Furnaces fur15 ON t.Refnum=fur15.Refnum AND t.StudyYear=2015
--WHERE LEN(ISNULL(fur.Refnum,''))>0
--AND (
--              fur.Refnum like '%PCH118' 
--              OR fur.Refnum like '%PCH179'
--       ) 
--AND t.Region like 'NA'
--GROUP BY 
--       fur.Refnum  
--       ,fur.FurnNo 
--       ,t.StudyYear
--       ,t.CoLoc
--       ,t.Region
--ORDER BY fur.Refnum ASC


DECLARE @StudyYear INT = 2015
, @counter INT = 2015

DECLARE @retube TABLE
(
	Refnum VARCHAR(50)
	,StudyYear INT
	,Coloc VARCHAR(50)
	,InflAdjAnn_MaintRetubeTot_Cur INT
	,Region VARCHAR(50)
	,ActCap INT
	,RetubeCostPerTon DECIMAL(18,2)
)


WHILE @Counter > (@StudyYear -9)
BEGIN

PRINT @StudyYear - (@StudyYear-@counter)

INSERT @retube
SELECT DISTINCT
       fur.refnum  
       ,t.StudyYear
       ,t.CoLoc
       ,(
			SELECT SUM(InflAdjAnn_MaintRetubeTot_Cur) 
			FROM Olefins.dbo.Furnaces 
			WHERE Refnum = fur.Refnum
			AND ISNULL(InflAdjAnn_MaintRetubeTot_Cur,0)>0
			AND ISNULL(ActCap,0)>0
		) [InflAdjAnn_MaintRetubeTot_Cur]
       ,t.Region
       ,(
			SELECT SUM(ActCap) 
			FROM Olefins.dbo.Furnaces 
			WHERE Refnum = fur.Refnum
			AND ISNULL(InflAdjAnn_MaintRetubeTot_Cur,0)>0
			AND ISNULL(ActCap,0)>0
		) [ActCap]
       ,SUM(furYr.InflAdjAnn_MaintRetubeTot_Cur)/(SUM(furYr.actcap)/1000)
FROM Olefins.dbo.Furnaces fur
FULL JOIN Olefins.dbo.TSort t ON fur.Refnum=t.Refnum
INNER JOIN Olefins.dbo.GENSUM g   ON g.Refnum=t.refnum
LEFT JOIN Olefins.dbo.Furnaces furYr ON t.Refnum=furYr.Refnum
AND ISNULL(furYr.InflAdjAnn_MaintRetubeTot_Cur,0)>0
AND ISNULL(furYr.ActCap,0)>0
WHERE LEN(ISNULL(fur.Refnum,''))>0
AND (
              fur.Refnum like '%PCH118' 
              OR fur.Refnum like '%PCH179'
       ) 
AND t.Region like 'NA'
AND t.StudyYear=@StudyYear - (@StudyYear-@counter)
AND ISNULL(fur.InflAdjAnn_MaintRetubeTot_Cur,0)>0
AND ISNULL(fur.ActCap,0)>0
GROUP BY 
       fur.Refnum 
       ,t.StudyYear
       ,t.CoLoc
       ,t.Region
	   ,fur.Refnum 

SELECT @Counter = @Counter - 2
END


SELECT * FROM @retube
--Where ISNULL(RetubeCostPerTon,0)>0
Order By Refnum
