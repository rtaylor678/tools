﻿/*
ESC
LSC
ED3
ED2
ED1
LB3
LB2
LB1
GAS2
GAS1
CE3
CE2
CE1
CG3
CG2
CG1
SC
*/

/***********************************************************************************
Description:      
Creates Maintenance Risk points over multiple years

Author: Rodrick Blanton
Date: 06 DEC 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/


CREATE FUNCTION [dbo].[MaintRiskTrend]
(
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)
)
RETURNS @Return TABLE
(
	UnitLabel VARCHAR(50),
	EUF2Yr REAL,
	AnnMaintCostMWH REAL
)
AS
BEGIN

	DECLARE
	@CompanyID VARCHAR(50)

	DECLARE @RefList TABLE (RefNum VARCHAR(50))
	DECLARE @Company TABLE (CompanyID VARCHAR(50))


	INSERT @Company
	SELECT CompanyID
	FROM TSort
	WHERE Refnum IN 
	(
		SELECT Refnum
		FROM _RL
		WHERE ListName = @ListName
	)

	INSERT @RefList
	SELECT r.Refnum
	FROM _RL r
	INNER JOIN TSort t ON r.Refnum = t.Refnum 
	WHERE r.ListName = @ShortListName + ' Trends'
	AND t.CompanyID IN
	(
		SELECT CompanyID
		FROM @Company
	)

	--SELECT * FROM @RefList



	INSERT @Return
	SELECT RTRIM(t.UnitLabel) + '_' + SUBSTRING(CONVERT(VARCHAR(4), t.StudyYear),3,2), nf.EUF2Yr, AnnMaintCostMWH = mtc.AnnMaintCostMWH * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, t.StudyYear)
	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	WHERE t.Refnum IN (SELECT Refnum FROM @RefList) AND t.StudyYear <= @StudyYear
		AND (b.CRVSizeGroup = @CRVGroup OR t.Refnum LIKE '%G')

	UNION


	-- Should we SUM here instead of doing a group by?  Slide only contains 1 Peer and PS
	SELECT 'Peer', GlobalDB.dbo.WtAvgNZ(nf.EUF2Yr, nf.WPH2Yr), GlobalDB.dbo.WtAvg( mtc.AnnMaintCostMWH, gtc.AdjNetMWH2Yr) 
	* GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
	--* GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, t.StudyYear)
	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.Refnum IN 
		(SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		--(SELECT Refnum FROM @RefList) AND t.StudyYear <= @StudyYear
		AND b.CRVSizeGroup = @CRVGroup
	--GROUP BY t.StudyYear

	UNION

	SELECT 'PS', GlobalDB.dbo.WtAvgNZ(nf.EUF2Yr, nf.WPH2Yr), GlobalDB.dbo.WtAvg( mtc.AnnMaintCostMWH, gtc.AdjNetMWH2Yr) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.Refnum IN 
		(SELECT Refnum FROM _RL WHERE ListName = (@ShortListName + ' Pacesetters')) AND t.StudyYear = @StudyYear
		--(SELECT Refnum FROM @RefList) AND t.StudyYear >= @StudyYear
		AND b.CRVSizeGroup = @CRVGroup


	RETURN

END