﻿/***********************************************************************************
Description:      
This script is to simplify adding a company to the RAM study.

Author: Rodrick Blanton
Date: 16 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		Create encrypted password by using RAM password creator here
		$/Sa.RAMStudy/RAMPWCreater
***********************************************************************************/



DECLARE
@CompanyName nvarchar(50) = '[COMPANY]'
, @CompanyID varchar(12) = '[COMPANYABR]'
, @StudyYear smallint = NULL
, @CoordPwd nvarchar(40) = '[ENCRYPTED PASSWORD]'
, @CoordFirstName nvarchar(20) = '[FIRST NAME]'
, @CoordLastName nvarchar(25) = '[LAST NAME]'
, @CoordEmail nvarchar(60) = '[EMAIL]'
, @CoordJobTitle nvarchar(55) = NULL
, @CoordPhone nvarchar(15) = NULL
, @SiteCntLimit smallint = NULL

/*
	Do not provide values for the parms below
*/
, @CompanySID varchar(15)
, @CoordUserID nvarchar(60)
, @CoordScreenName nvarchar(30)


SELECT @CompanySID = @CompanyID + LEFT(RIGHT(CONVERT(VARCHAR(7), GETDATE(), 120), 5),2)


SELECT 
@CoordScreenName = @CoordFirstName + '.' + @CoordLastName
, @CoordUserID = @CoordFirstName + '.' + @CoordLastName + '/' + @CompanySID


SELECT
@CompanyName
, @CompanyID
, @StudyYear
, @CoordPwd
, @CoordFirstName
, @CoordLastName
, @CoordEmail
, @CoordJobTitle
, @CoordPhone
, @SiteCntLimit
, @CompanySID
, @CoordUserID
, @CoordScreenName


--EXEC AddCompany 
--	@CompanyName, @CompanySID, @CompanyID, @StudyYear,
--	@CoordUserID, @CoordPwd, @CoordFirstName, @CoordLastName,
--	@CoordEmail, @CoordJobTitle, @CoordPhone, @CoordScreenName,
--	@SiteCntLimit