﻿/***********************************************************************************
Description:      
As of 08 Aug 2017 this proc is not in use.  The purpose of this table is to the EGC
calculations.  It's based on a 2013 version of the proc in PowerWork of the same name.

Author: Rodrick Blanton
Date: 03 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/



DECLARE @Refnum VARCHAR(12)
,@year int = 2015
,@DEBUG bit = 1
,@exceptionName VARCHAR(50) = ''

--SET @Refnum = 'TJS110P15' --KEGC 36.49

SET @Refnum = '718133P15' --KEGC 28.90

--EXEC spEGCCalcs2013 @Refnum, @year




	SET NOCOUNT ON
	SET @refnum = rtrim(@refnum)

	
	-----------------------DECLARE all the variables used for the EGC totals-----------------------
	DECLARE @sessionID UNIQUEIDENTIFIER

	SELECT @sessionID = NEWID()

	INSERT EGCTotals (ID, StudyYear, RefNum)
	SELECT @sessionID, @year, @Refnum



	DECLARE @ModelData TABLE
	(
		ID INT NOT NULL 
		,Model CHAR(12) DEFAULT('') NOT NULL
		,IsRankine BIT DEFAULT(0) NOT NULL
		,RankineCoal BIT DEFAULT(0) NOT NULL
		,RankineGas BIT DEFAULT(0) NOT NULL
		,CC BIT DEFAULT(0) NOT NULL
		,SC BIT DEFAULT(0) NOT NULL
	)

	-- Initialize row in table
	INSERT @ModelData(ID)
	SELECT 1
	
	
	----------------DECLARE all the variables used to hold the unit data----------------------	
	DECLARE @dataGrab TABLE
	(
		  StudyYear INT NOT NULL
		, Age REAL NULL
		, NDC REAL NULL
		, CTG_Count REAL NULL
		, CTG_NDC REAL NULL
		, NumUnitsAtSite REAL NULL
		, Site_NDC REAL NULL
		, Frequency REAL NULL
		, FuelType CHAR(8) NULL
		, PrecBagYN REAL NULL
		, ScrubbersYN REAL NULL
		, DesignEffPct REAL NULL
		, SCR REAL NULL
		, BlrPSIG REAL NULL
		, ServiceHrs REAL NULL
		, AdjNetMWH REAL NULL
		, AdjNetMWH2Yr REAL NULL
		, Starts REAL NULL
		, SteamSalesMBTU REAL NULL
		, DieselJet REAL NULL
		, HeavyFuel REAL NULL
		, NatGasMBTU REAL NULL
		, OffGasMBTU REAL NULL
		, Coal1Tons REAL NULL
		, Coal1AshPcnt REAL NULL
		, Coal1SulfurPcnt REAL NULL
		, Coal1MoisturePcnt REAL NULL
		, Coal2Tons REAL NULL
		, Coal2AshPcnt REAL NULL
		, Coal2SulfurPcnt REAL NULL
		, Coal2MoisturePcnt REAL NULL
		, Coal3Tons REAL NULL
		, Coal3AshPcnt REAL NULL
		, Coal3SulfurPcnt REAL NULL
		, Coal3MoisturePcnt REAL NULL
		, NumBoilers REAL NULL
		, BoilerSizeFactor REAL NULL
		, NumSTs REAL NULL
		, TotNumMajEqpt REAL NULL
		, Class CHAR(4) NULL
		, SingleShaft REAL NULL
		, CTGGasMBTU REAL NULL
		, CTGLiquidMBTU REAL NULL
		, SiteMBTU REAL NULL
		, Taxes REAL NULL
		, CRVGroup VARCHAR(50) NULL
		, CRVSizeGroup VARCHAR(50) NULL
		--, CTGAge REAL NULL -- added 2/3/16 SW
		, POHours REAL NULL -- added 7/15/16 SW
	)


	DECLARE @RefNumRegEx VarChar(14)
	
	SELECT @RefNumRegEx = CASE WHEN @refnum LIKE '%C[0-9][0-9]' THEN '%C[0-9][0-9]'
							   WHEN @refnum LIKE '%C[0-9][0-9]P' THEN '%C[0-9][0-9]P'
						  ELSE
							  '%CC%'	
						  END

	
	--------------------Initial Data Grab----------------------------
	INSERT @dataGrab
	SELECT 
		@year,
		CASE WHEN nt_CTGAge.CTGAge < nf.Age THEN nt_CTGAge.CTGAge ELSE Age END, -- @Age = 
		nf.NDC, --@NDC = 
		CASE WHEN t.Refnum LIKE @RefNumRegEx THEN ISNULL(ISNULL(nt_CTG2.CTG_Count,nt_CTG.CTG_Count),1) ELSE 0 END, --theory is that this is set up as a single shaft unit, so 1 CTG if NULL --@CTG_Count = 
		CASE WHEN t.refnum LIKE @RefNumRegEx THEN ROUND(ISNULL(nt_CTG.CTG_NDC, nf.NDC * 2/3),1) ELSE 0 END ,-- if it is a single shaft the CTG is producing 2/3 and the STG 1/3 --@CTG_NDC = 
		CASE WHEN t.Refnum LIKE @RefNumRegEx THEN ISNULL(p.CCUnitNum,0) + ISNULL(p.SCNum,0) + ISNULL(p.OthUnitNum,0) ELSE ISNULL(p.CoalUnitNum,0) + ISNULL(p.OilGasUnitNum,0) END, -- @NumUnitsAtSite = 
		CASE WHEN t.Refnum LIKE @RefNumRegEx THEN ROUND(ISNULL(p.CCUnitCap,0) + ISNULL(p.SCCap,0) + ISNULL(p.OthUnitCap,0),1) ELSE ISNULL(p.CoalUnitCap,0) + ISNULL(p.OilGasUnitCap,0) END, --@Site_NDC = 
		clu.FrequencyHz, --@Frequency = 
		ftc.FuelType, --@FuelType = 
		ISNULL(ps.PrecBagYN,0), --@PrecBagYN = 
		ISNULL(ps.ScrubbersYN,0), --@ScrubbersYN = 
		CASE ISNULL(ps.ScrubbersYN,0) WHEN 0 THEN 0 ELSE CASE ISNULL(ms.DesignEffPct,0) WHEN 0 THEN 94 ELSE ISNULL(ms.DesignEffPct,0) END END, --removes the zeroes, 94 decided by consultants as the default value after looking at data --@DesignEffPct = 
		CASE WHEN scrs.HasSCR IS NULL THEN ISNULL(pscc.SCR,0) ELSE CASE scrs.HasSCR WHEN 0 THEN 0 ELSE 1 END END, --@SCR = 
		ISNULL(ec.BoilerPressure, ISNULL(dd.BlrPSIG,0)), --@BlrPSIG = 
		nf.ServiceHrs, -- @ServiceHrs = 
		gtc.AdjNetMWH, --@AdjNetMWH = 
		gtc.AdjNetMWH2Yr, --@AdjNetMWH2Yr = 
		CASE ISNULL(nf.ACTStarts,0) WHEN 0 THEN ISNULL(gtc.TotStarts,0) ELSE ISNULL(nf.ACTStarts,0) END, --@Starts = 
		ISNULL(ss.StmSalesMBTU,0), --@SteamSalesMBTU = 
		ISNULL(DieselMBTU,0) + ISNULL(JetTurbMBTU,0), --@DieselJet = 
		ISNULL(FuelOilMBTU,0), --@HeavyFuel = 
		ISNULL(NatGasMBTU,0) + ISNULL(H2GasMBTU,0), --@NatGasMBTU = 
		ISNULL(OffGasMBTU,0), --@OffGasMBTU = 
		ISNULL(c1.Tons, 0), --@Coal1Tons = 
		ISNULL(c1.AshPcnt, 0), --@Coal1AshPcnt = 
		ISNULL(c1.SulfurPcnt, 0), --@Coal1SulfurPcnt = 
		ISNULL(c1.MoistPcnt,0), --@Coal1MoisturePcnt = 
		ISNULL(c2.Tons, 0), --@Coal2Tons = 
		ISNULL(c2.AshPcnt, 0), --@Coal2AshPcnt = 
		ISNULL(c2.SulfurPcnt, 0), --@Coal2SulfurPcnt = 
		ISNULL(c2.MoistPcnt,0), --@Coal2MoisturePcnt = 
		ISNULL(c3.Tons, 0), --@Coal3Tons = 
		ISNULL(c3.AshPcnt, 0), --@Coal3AshPcnt = 
		ISNULL(c3.SulfurPcnt, 0), --@Coal3SulfurPcnt = 
		ISNULL(c3.MoistPcnt, 0), --@Coal3MoisturePcnt = 
		ISNULL(ec.NumBoilers, ISNULL(blr.NumBoilers,0)), --@NumBoilers = 
		CASE 
			WHEN ISNULL(c.HeatValue,0) = 0 THEN 0
			WHEN ISNULL(c.HeatValue,0) < 8300 THEN 2.03 --magic numbers determined by Tony and Rick 5/14
			WHEN ISNULL(c.HeatValue,0) > 9500 THEN 1
			ELSE 1.21 -- 8300-9500
		END, --@BoilerSizeFactor = 
		ISNULL(ec.NumSTGs, ISNULL(sts.NumSTs,0)), --@NumSTs = 
		CASE WHEN t.Refnum LIKE @RefNumRegEx THEN 
				(ISNULL(ctgs.NumCTGs,0) * 2) + ISNULL(blr.NumBoilers,0) + (ISNULL(sts.NumSTs,0) * 2) + ISNULL(hrsgs.NumHRSGs,0) - ISNULL(t.SingleShaft,0)
			 ELSE ISNULL(ctgs.NumCTGs,0) + ISNULL(blr.NumBoilers,0) + (ISNULL(sts.NumSTs,0) * 2) END, --@TotNumMajEqpt = 

		ctgd.Class, --@Class = 
		ISNULL(t.SingleShaft,0), -- this needs to be fixed somehow --@SingleShaft = 
		fuel.CTGGasMBTU, --@CTGGasMBTU = 
		fuel.CTGLiquidMBTU, --@CTGLiquidMBTU = 
		fuel.SiteMBTU, --@SiteMBTU = 
		o.OthTax + o.PropTax, --2014 addition --@Taxes = 
		b.CRVGroup,
		b.CRVSizeGroup,
		CASE WHEN PO.eventyear = t.evntyear THEN PO.POHours ELSE 0 END -- 2015 addition --@POHours = 

	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*), CTG_NDC = SUM(CASE WHEN TurbineType = 'CC' THEN (NDC * 2/3) ELSE NDC END) -- trying to handle blocks
					FROM NERCTurbine 
					WHERE TurbineType IN( 'CTG','CC') AND NDC IS NOT NULL 
					GROUP BY Refnum) nt_CTG ON nt_CTG.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, CTGAge = MAX(Age) FROM NERCTurbine WHERE TurbineType = 'CTG' GROUP BY Refnum) nt_CTGAge ON nt_CTGAge.Refnum = t.Refnum
						
		LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*) FROM NERCTurbine WHERE TurbineType = 'CTG' AND NDC IS NULL GROUP BY Refnum) nt_CTG2 ON nt_CTG2.Refnum = t.Refnum
		--this one is trying to handle the (very rare, perhaps only Capital Bridgeport?) unit that has a CC set up with an NDC, and CTGs set up but NULL, to get the correct CTG count
		LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
		LEFT JOIN PowerGlobal.dbo.Country_LU clu ON clu.Country = t.Country
		LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
		LEFT JOIN Misc ms ON ms.Refnum = t.Refnum AND ms.TurbineID = 'STG'
		LEFT JOIN CoalTotCalc c ON c.Refnum = t.Refnum
		LEFT JOIN (select Refnum, NumBoilers = SUM(NumBoilers), NumSTGs = SUM(NumSTGs), BoilerPressure = MAX(BoilerPressure) from EquipCount group by Refnum) ec ON ec.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, FiringTemp = MAX(FiringTemp), Class FROM CTGData GROUP BY Refnum, Class) ctgd ON ctgd.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, --SW 6/10/14 going back to looking at Equipment table, trusting consultants to check it is correct
						PrecBagYN = CASE SUM(CASE WHEN EquipType IN ('BAG','PREC') THEN 1 ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END,
						ScrubbersYN = CASE SUM(CASE WHEN EquipType IN ('WGS','DGS') THEN 1 ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END--,
						--SCR = CASE SUM(CASE WHEN EquipType IN ('SCR','CTG-SCR') THEN 1 ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END
					--FROM (SELECT DISTINCT Refnum, EquipType FROM Equipment WHERE EquipType IN ('BAG','CTG-SCR','DGS','PREC','SCR','WGS')) b GROUP BY Refnum) ps ON ps.Refnum = t.Refnum
					FROM (SELECT DISTINCT Refnum, EquipType FROM Equipment WHERE EquipType IN ('BAG','DGS','PREC','WGS')) b GROUP BY Refnum) ps ON ps.Refnum = t.Refnum

		--LEFT JOIN (SELECT Refnum, 
		--				PrecBagYN = CASE SUM(CASE WHEN Component IN ('PREC','BAG') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END,
		--				ScrubbersYN = CASE SUM(CASE WHEN Component IN ('WGS','DGS') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END,
		--				SCR = CASE SUM(CASE WHEN Component IN ('SCR') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END
		--			FROM (SELECT Refnum, Component, TotAnnOHCost = ISNULL(TotAnnOHCost,0) FROM OHMaint UNION SELECT Refnum, Component, AnnNonOHCost = ISNULL(AnnNonOHCost,0) FROM NonOHMaint UNION SELECT Refnum, 'WGS', ScrCostLime FROM Misc WHERE ScrCostLime IS NOT NULL) b -- the Misc part calcs the scrubber cost, set to 'WGS' just for it to count from
		--			WHERE Component IN ('BAG','PREC','WGS','DGS','SCR')
		--			GROUP BY Refnum) ps ON ps.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, 
						SCR = CASE SUM(CASE WHEN Component IN ('SCR','CTG-SCR') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END
					FROM (SELECT Refnum, Component, TotAnnOHCost = ISNULL(TotAnnOHCost,0) FROM OHMaint UNION SELECT Refnum, Component, AnnNonOHCost = ISNULL(AnnNonOHCost,0) FROM NonOHMaint) b
					WHERE Component IN ('CTG-SCR','SCR')
					GROUP BY Refnum) pscc ON pscc.Refnum = t.Refnum
					
		LEFT JOIN (SELECT nt.Refnum, BlrPSIG = MAX(ISNULL(d.BlrPSIG,0)) FROM NERCTurbine nt LEFT JOIN DesignData d ON d.UtilityUnitCode = nt.UtilityUnitCode GROUP BY nt.Refnum) dd ON dd.Refnum = t.Refnum
									
		LEFT JOIN (SELECT s.refnum, StmSalesMBTU = SUM(StmSalesMBTU), ABSStmSalesMBTU = SUM(ABS(StmSalesMBTU)) FROM SteamSales s LEFT JOIN TSort t ON t.Refnum = s.Refnum WHERE YEAR(s.Period) = t.StudyYear GROUP BY s.Refnum) ss ON ss.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, AshPcnt = MAX(ISNULL(AshPcnt,0)), SulfurPcnt = MAX(ISNULL(SulfurPcnt,0)), MoistPcnt = MAX(ISNULL(MoistPcnt,0)), Tons = SUM(ISNULL(Tons,0)), MBTU = SUM(ISNULL(MBTU,0)) FROM Coal WHERE CoalID = '1' GROUP BY Refnum) c1 ON c1.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, AshPcnt = MAX(ISNULL(AshPcnt,0)), SulfurPcnt = MAX(ISNULL(SulfurPcnt,0)), MoistPcnt = MAX(ISNULL(MoistPcnt,0)), Tons = SUM(ISNULL(Tons,0)), MBTU = SUM(ISNULL(MBTU,0)) FROM Coal WHERE CoalID = '2' GROUP BY Refnum) c2 ON c2.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, AshPcnt = MAX(ISNULL(AshPcnt,0)), SulfurPcnt = MAX(ISNULL(SulfurPcnt,0)), MoistPcnt = MAX(ISNULL(MoistPcnt,0)), Tons = SUM(ISNULL(Tons,0)), MBTU = SUM(ISNULL(MBTU,0)) FROM Coal WHERE CoalID = '3' GROUP BY Refnum) c3 ON c3.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, NumBoilers = COUNT(*) FROM Equipment WHERE EquipType = 'BOIL' GROUP BY Refnum) blr ON blr.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, NumSTs = COUNT(*) FROM Equipment WHERE EquipType = 'STG' GROUP BY Refnum) sts ON sts.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, NumCTGs = COUNT(*) FROM Equipment WHERE EquipType = 'CTG-GEN' GROUP BY Refnum) ctgs ON ctgs.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, NumHRSGs = COUNT(*) FROM Equipment WHERE EquipType = 'HRSG' GROUP BY Refnum) hrsgs ON hrsgs.Refnum = t.Refnum


		--LEFT JOIN (SELECT Refnum, NumGens = COUNT(*) FROM Equipment WHERE EquipType = 'CTG-GEN' GROUP BY Refnum) ctgs ON ctgs.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, HasSCR = COUNT(*) FROM Equipment WHERE EquipType = 'SCR' GROUP BY Refnum) scrs ON scrs.Refnum = t.Refnum --updated for new equipment lists in 2014 study
		
		LEFT JOIN (SELECT Refnum,
							SiteMBTU = SUM(CASE TurbineID WHEN 'Site' THEN MBTU ELSE 0 END),
							CTGLiquidMBTU = SUM(CASE TurbineID WHEN 'Site' THEN 0 ELSE CASE WHEN FuelType IN ('Diesel','Jet','FOil') THEN MBTU ELSE 0 END END),
							CTGGasMBTU = SUM(CASE TurbineID WHEN 'Site' THEN 0 ELSE CASE WHEN FuelType IN ('Diesel','Jet','FOil') THEN 0 ELSE MBTU END END)
					FROM Fuel
					GROUP BY Refnum) fuel ON fuel.Refnum = t.Refnum AND t.Refnum LIKE @RefNumRegEx

		LEFT JOIN (SELECT Refnum, PropTax = ISNULL(PropTax,0), OthTax = ISNULL(OthTax,0) FROM OpExCalc WHERE DataType = 'ADJ') o ON o.Refnum = t.Refnum

		LEFT JOIN (SELECT nt.Refnum, eventyear = YEAR(TL_DateTime), POHours = SUM(PO)/COUNT(*)
					FROM NERCTurbine nt
						LEFT JOIN GADSOS.GADSNG.Setup s ON s.UtilityUnitCode = nt.UtilityUnitCode
						LEFT JOIN GADSOS.GADSNG.EventHours e on e.UnitShortName = s.UnitShortName AND e.Granularity = 'Yearly'
					GROUP BY nt.Refnum, year(TL_DateTime)) PO ON PO.Refnum = t.Refnum and PO.eventyear = t.EvntYear

	WHERE t.Refnum = @refnum


	/*
	*/
	UPDATE EGCTotals
	SET EGC_Multiplier = ISNULL
					(
						(
							SELECT CASE
										WHEN isDiviser = 0 THEN bias
								   ELSE
										1
								   END
							FROM EGCCRVBias e
							INNER JOIN @dataGrab d ON d.CRVGroup = e.crvGroupID
								AND d.CRVSizeGroup = e.crvSizeGroup
								AND d.StudyYear = e.StudyYear
						), 1
					)
	, EGC_Diviser = ISNULL
					(
						(
							SELECT CASE
										WHEN isDiviser = 1 THEN bias
								   ELSE
										1
								   END
							FROM EGCCRVBias e
							INNER JOIN @dataGrab d ON d.CRVGroup = e.crvGroupID
								AND d.CRVSizeGroup = e.crvSizeGroup
								AND d.StudyYear = e.StudyYear
						), 1
					)
	WHERE ID = @sessionID



		
	----------------------populate the setup variables
	--IF @refnum NOT LIKE '%CC%  SOME REFNUM ONLY HAS ONE C FOLLOWED BY yy OR yyP'
	IF @refnum NOT LIKE   @RefNumRegEx
	BEGIN 
	PRINT @Refnum
		 Update @ModelData SET IsRankine = 1
		IF (SELECT FuelType FROM @dataGrab) = 'Coal'
		BEGIN
			Update @ModelData SET RankineCoal = 1
			Update @ModelData SET Model = 'COAL'
		END
		IF (SELECT FuelType FROM @dataGrab) <> 'Coal'
		BEGIN
			Update @ModelData SET RankineGas = 1
			Update @ModelData SET Model = 'GO'
		END
	END
	ELSE
	BEGIN
		Update @ModelData SET IsRankine = 0
		IF (SELECT NDC - CTG_NDC FROM @dataGrab) < 1 -- should be the same, but slight fudge factor for rounding
		BEGIN
			Update @ModelData SET SC = 1
			Update @ModelData SET Model = 'SC'
		END
		ELSE
		BEGIN
			Update @ModelData SET CC = 1
			Update @ModelData SET Model = 'CC'
		END
	END


	--IF @refnum NOT LIKE '%CC%  SOME REFNUM ONLY HAS ONE C FOLLOWED BY yy OR yyP'
	IF (@refnum NOT LIKE @RefNumRegEx)
	BEGIN
		IF (SELECT FuelType FROM @dataGrab) = 'Oil'
		BEGIN
			Update @ModelData SET RankineGas = 1
			Update @ModelData SET Model = 'GO'
		END
	END
	

	--------Scrubbers--------
	UPDATE EGCTotals
	SET EGC_Scrubber =
	(
		SELECT TOP 1
				(
				POWER(d.ScrubbersYN * d.DesignEffPct * ((d.Coal1Tons * d.Coal1SulfurPcnt) + (d.Coal2Tons * d.Coal2SulfurPcnt) + (d.Coal3Tons * d.Coal3SulfurPcnt))/1000000 ,e.Pwr_Scrubber) * e.Mult_Scrubber
				)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID

	

	--------PrecBag--------
	UPDATE EGCTotals
	SET EGC_PrecBag =
	(
		SELECT TOP 1 
			(
				POWER( d.PrecBagYN * CASE WHEN ((d.Coal1Tons * d.Coal1AshPcnt) + (d.Coal2Tons * d.Coal2AshPcnt) + (d.Coal3Tons * d.Coal3AshPcnt)) = 0 THEN 0 ELSE LOG((d.Coal1Tons * d.Coal1AshPcnt) + (d.Coal2Tons * d.Coal2AshPcnt) + (d.Coal3Tons * d.Coal3AshPcnt)) END ,e.Pwr_PrecBag) * e.Mult_PrecBag
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------SCR--------
	UPDATE EGCTotals
	SET EGC_SCR =
	(
		SELECT TOP 1 
			( 
				POWER(d.SCR * (CASE (SELECT TOP 1 Model FROM @ModelData) WHEN 'SC' THEN d.AdjNetMWH ELSE d.AdjNetMWH2Yr END / CASE (SELECT TOP 1 Model FROM @ModelData) WHEN 'SC' THEN CASE WHEN d.StudyYear = 2013 THEN 1 ELSE 1000 END WHEN 'CC' THEN CASE WHEN d.StudyYear < 2015 THEN 1000000 ELSE 1000 END ELSE 1000000 END), e.Pwr_SCR) * e.Mult_SCR
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID

	
	--------Frequency50--------
	UPDATE EGCTotals
	SET EGC_Frequency50 = 
	(
		SELECT TOP 1 
			(
				CASE d.Frequency WHEN 50 THEN 1 ELSE 0 END  
				* POWER(CASE (SELECT TOP 1 Model FROM @ModelData) WHEN 'CC' THEN case WHEN @year < 2015 THEN d.AdjNetMWH/1000000 ELSE d.NDC END ELSE d.NDC END, e.Pwr_Freq50) * e.Mult_Freq50
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------Frequency60--------
	UPDATE EGCTotals
	SET EGC_Frequency60 = 
	(
		SELECT TOP 1 
			(
				(SELECT TOP 1 IsRankine FROM @ModelData)
				* CASE d.Frequency WHEN 60 THEN 1 ELSE 0 END  -- this is Freq60
				* POWER(d.NDC, e.Pwr_Freq60)
				* e.Mult_Freq60
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID



	--------Starts--------
	UPDATE EGCTotals
	SET EGC_Starts =
	(
		SELECT TOP 1 
			(
				CASE WHEN (SELECT TOP 1 IsRankine FROM @ModelData) = 0 THEN 1 ELSE 0 END -- Rankine units get nothing, non-Rankine units get calced
				* POWER((d.ServiceHrs/CASE WHEN d.Starts > 0 THEN d.Starts ELSE 1 END), e.Pwr_Starts) -- case stmt to fix anyone with 0 starts
				* e.Mult_Starts
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID



	--------Site Effect--------
	UPDATE EGCTotals
	SET EGC_SiteEffect =
	(
		SELECT TOP 1 
			(
				CASE WHEN (SELECT TOP 1 CC FROM @ModelData) <> 1 OR d.StudyYear < 2015 THEN
					CASE WHEN (SELECT TOP 1 IsRankine FROM @ModelData) = 1 THEN SIGN(d.NDC/d.Site_NDC-0.5) ELSE SIGN(d.NDC - d.Site_NDC/d.NumUnitsAtSite) END
					* POWER(ABS(CASE WHEN (SELECT TOP 1 IsRankine FROM @ModelData) = 1 THEN d.NDC/d.Site_NDC-0.5 ELSE d.NDC - d.Site_NDC/d.NumUnitsAtSite END), e.Pwr_SiteEffect)
					* e.Mult_SiteEffect
				ELSE 
					POWER(d.NDC/(d.Site_NDC/d.NumUnitsAtSite),  e.Pwr_SiteEffect) * e.Mult_SiteEffect
				END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID



	--------Other Site Cap--------
	UPDATE EGCTotals
	SET EGC_OtherSiteCap = 
	(
		SELECT TOP 1 
			(
				CASE WHEN (SELECT TOP 1 IsRankine FROM @ModelData) <> 1 AND d.StudyYear < 2015 THEN
					(SELECT TOP 1 IsRankine FROM @ModelData)
					* SIGN(d.NDC - d.Site_NDC/d.NumUnitsAtSite) 
					* POWER(ABS(d.NDC - d.Site_NDC/d.NumUnitsAtSite), e.Pwr_OtherSiteCap)
					* e.Mult_OtherSiteCap
				ELSE 
					POWER ((d.NDC/(d.Site_NDC/d.NumUnitsAtSite)), e.Pwr_OtherSiteCap)
					* e.Mult_OtherSiteCap
				END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------Steam Sales--------
	UPDATE EGCTotals
	SET EGC_SteamSales =  
	(
		SELECT TOP 1 
			(
				CASE WHEN (SELECT TOP 1 CC FROM @ModelData) = 1 THEN POWER(LOG(1 + ABS(d.SteamSalesMBTU/case when d.StudyYear <2015 then 1000000 else 1000 end)), e.Pwr_SteamSalesMBTU)
					ELSE POWER(d.SteamSalesMBTU/(CASE WHEN (SELECT TOP 1 IsRankine FROM @ModelData) = 1 THEN 1000 ELSE 1000000 END), e.Pwr_SteamSalesMBTU) END
				* e.Mult_SteamSalesMBTU
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------Solid Fuel--------
	UPDATE EGCTotals
	SET EGC_SolidFuel =  
	(
		SELECT TOP 1 
			(
				(SELECT TOP 1 RankineCoal FROM @ModelData) * --only applies to Coal

				POWER((d.Coal1Tons * (1 - (d.Coal1AshPcnt + d.Coal1SulfurPcnt + d.Coal1MoisturePcnt)/100) +
						d.Coal2Tons * (1 - (d.Coal2AshPcnt + d.Coal2SulfurPcnt + d.Coal2MoisturePcnt)/100) +
						d.Coal3Tons * (1 - (d.Coal3AshPcnt + d.Coal3SulfurPcnt + d.Coal3MoisturePcnt)/100))/1000
					,e.Pwr_SolidFuel)
				* e.Mult_SolidFuel
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------NatGas + H2Gas--------
	UPDATE EGCTotals
	SET EGC_NatGasH2Gas = 
	(
		SELECT TOP 1 
			(
				(SELECT TOP 1 IsRankine FROM @ModelData) *
				POWER(d.NatGasMBTU/1000, e.Pwr_NatGas)
				* e.Mult_NatGas
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------OffGas--------
	UPDATE EGCTotals
	SET EGC_OffGas =
	(
		SELECT TOP 1 
			(
				(CASE WHEN d.StudyYear = 2013 THEN (SELECT TOP 1 RankineGas FROM @ModelData) ELSE (SELECT TOP 1 IsRankine FROM @ModelData) END) *
				POWER(d.OffGasMBTU/1000, e.Pwr_OffGas)
				* e.Mult_OffGas
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------Diesel + Jet--------
	UPDATE EGCTotals
	SET EGC_DieselJet = 
	(
		SELECT TOP 1 
			(
				(CASE WHEN d.StudyYear = 2013 THEN (SELECT TOP 1 RankineCoal FROM @ModelData) ELSE (SELECT TOP 1 IsRankine FROM @ModelData) END) * --changed 2014
				POWER(d.DieselJet/1000, e.Pwr_DieselJet)
				* e.Mult_DieselJet
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------HeavyFuel--------
	UPDATE EGCTotals
	SET EGC_HeavyFuel = 
	(
		SELECT TOP 1 
			(
				(SELECT TOP 1 RankineCoal FROM @ModelData) *
				POWER(d.HeavyFuel/1000, e.Pwr_HeavyFuel)
				* e.Mult_HeavyFuel
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


--	--------Age--------
--		--Rankine Coal has one equation, CC has another, the other two variants return 0.
--		--We add the Rankine Coal and CC together to get a result we can multiply both of them by, since they will only return a number for their own equation
--		--MAGIC NUMBER ALERT!
--	SET @EGC_Age =
--		(CASE WHEN @RankineCoal = 1 THEN POWER(@Age, @Pwr_Age) ELSE 0 END
--		+ CASE WHEN @CC = 1 THEN POWER(CASE WHEN @year = 2013 THEN 1.38898405948581 ELSE 1.91429722806757 END,(FLOOR(@Age/8)))* POWER(@NDC,@Pwr_Age) ELSE 0 END  -- changed 2014
--		+ CASE WHEN @SC = 1 THEN CASE @year WHEN 2013 THEN 0 WHEN 2014 THEN POWER (POWER(1.3000, (FLOOR(@Age/8))) * @CTG_NDC, @Pwr_Age) ELSE POWER(@Age, @Pwr_Age) END ELSE 0 END --changed 2014, 2015
--		)
--		* @Mult_Age

--	--a different multiplier for age when it is liquid model
--	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
--	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
--	BEGIN
--		SET @EGC_Age = 
--			POWER(1.91429722806757,(FLOOR(@Age/5)))* POWER(@NDC,@Pwr_Age)
--			* @Mult_Age

--	END



	
	--------Equiv Boiler--------
	UPDATE EGCTotals
	SET EGC_EquivBoiler = 
	(
		SELECT TOP 1 
			(
				(SELECT TOP 1 RankineCoal FROM @ModelData) *
				POWER(d.NumBoilers * d.BoilerSizeFactor,e.Pwr_EquivBoilers)
				* e.Mult_EquivBoilers
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	--------Supercritical--------
	UPDATE EGCTotals
	SET EGC_Supercritical = 
	(
		SELECT TOP 1 
			(
				(SELECT TOP 1 RankineCoal FROM @ModelData) *
				POWER(CASE WHEN d.BlrPSIG >= 3200 THEN d.NDC ELSE 0 END, e.Pwr_Supercritical)
				* e.Mult_Supercritical
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


--	--------Subcritical--------
--	SET @EGC_Subcritical = 
--	(CASE WHEN @year = 2013 THEN 0 ELSE @IsRankine END) * --added 2014
--		ISNULL(POWER(CASE WHEN @BlrPSIG < 3200 THEN @NDC/(CASE WHEN @RankineCoal = 1 THEN 1 WHEN @RankineGas = 1 THEN @Frequency ELSE 1 END) ELSE 0 END, ISNULL(@Pwr_Subcritical,0)),0)
--		* ISNULL(@Mult_Subcritical,0)

--	IF @RankineGas = 1 AND @year >= 2015 -- change to Subcritical for Rankine Gas in 2015, this will overwrite the value just above for the appropriate units
--	BEGIN
--		SET @EGC_Subcritical =
--		ISNULL(POWER (CASE WHEN @BlrPSIG < 3200 THEN @NDC ELSE 0 END, @Pwr_Subcritical),0)
--		* ISNULL(@Mult_Subcritical,0)
--	END



	--------MWH--------
	UPDATE EGCTotals
	SET EGC_MWH = 
	(
		SELECT TOP 1 
			(
				(CASE WHEN d.StudyYear = 2013 THEN 0 ELSE (SELECT TOP 1 RankineGas FROM @ModelData) END ) * --added 2014
				POWER(d.AdjNetMWH2Yr/1000000, ISNULL(e.Pwr_MWH,0))
				* ISNULL(e.Mult_MWH,0)
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID

	--------Taxes--------
	--in 2014 we calced the model against the non-Tax TCLFE total, but we need to add the value of taxes back in to compare against the taxed TCLFE total
	--we simply add the tax amount, because it will count 100% against EGC
	UPDATE EGCTotals
	SET EGC_Taxes = 
	(
		SELECT TOP 1 
			(
				CASE WHEN d.StudyYear = 2013 THEN 0 ELSE d.Taxes END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID


	---------PO Hours----------------
	UPDATE EGCTotals
	SET EGC_POHours =
	(
		SELECT TOP 1 
			(
				(CASE WHEN d.StudyYear < 2015 THEN 0 ELSE 1 END) * --added for 2015
				POWER(d.POHours/168, ISNULL(e.Pwr_POHours,0)) --divide by 168 because we want the number of weeks
				* ISNULL(e.Mult_POHours,0)
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID

	--------- Service Hours ----------------
	UPDATE EGCTotals
	SET EGC_ServiceHours = 
	(
		SELECT TOP 1 
			(
				CASE WHEN (SELECT TOP 1 CC FROM @ModelData) = 1 AND d.StudyYear >= 2015 THEN
					POWER(d.ServiceHrs, e.Pwr_ServiceHours)
					* e.Mult_ServiceHours
				ELSE
					0
				END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID

	--------- HRSG Capacity ----------------
	UPDATE EGCTotals
	SET EGC_HRSGCap = 
	(
		SELECT TOP 1 
			(
				CASE WHEN (SELECT TOP 1 CC FROM @ModelData) = 1 AND d.StudyYear >= 2015 THEN
					POWER(((d.NDC - d.CTG_NDC)/d.CTG_Count), e.Pwr_HRSGCap)
					* e.Mult_HRSGCap
				ELSE
					0
				END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID



	---------------- HRSG Capacity N - added 2015 for liquid fuel units only ----------------
	UPDATE EGCTotals
	SET EGC_HRSGCapN = 
	(
		SELECT TOP 1 
			(
				CASE WHEN d.StudyYear >= 2015 AND (SELECT TOP 1 Model FROM @ModelData) = 'CC' AND (d.CTGLiquidMBTU/(d.CTGLiquidMBTU + d.CTGGasMBTU) > 0.75) THEN
					POWER((100 * ((d.NDC - d.CTG_NDC)/d.NDC)), e.Pwr_HRSGCapN)
					* e.Mult_HRSGCapN
				ELSE
					0
				END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID

	---------------- CTG Liquid fuel, added 2015 for liquid fuel units only ----------------
	UPDATE EGCTotals
	SET EGC_CTGLiquidFuel = 
	(
		SELECT TOP 1 
			(
				CASE WHEN d.StudyYear >= 2015 AND (SELECT TOP 1 Model FROM @ModelData) = 'CC' AND (d.CTGLiquidMBTU/(d.CTGLiquidMBTU + d.CTGGasMBTU) > 0.75) THEN
					POWER((d.CTGLiquidMBTU/1000), e.Pwr_CTGLiquidFuel)
					* e.Mult_CTGLiquidFuel
				ELSE
					0
				END
			)
		FROM @dataGrab d
		FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
		AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
		AND e.ExceptionName = @exceptionName
	)
	WHERE ID = @sessionID




	IF (SELECT Top 1 IsRankine FROM @ModelData) = 0
		BEGIN

			--------CTG Gas--------
			UPDATE EGCTotals
			SET EGC_CTGGas = 
			(
				SELECT TOP 1 
					(
						POWER(CASE WHEN d.StudyYear = 2013 THEN d.CTGGasMBTU ELSE (d.NatGasMBTU + d.OffGasMBTU) END /CASE WHEN d.StudyYear < 2015 THEN 1000000 ELSE 1000 END, e.Pwr_CTGGas) --change 2014
						* e.Mult_CTGGas
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID


			--------Site Fuel--------
			UPDATE EGCTotals
			SET EGC_SiteFuel = 
			(
				SELECT TOP 1 
					(
						POWER(d.SiteMBTU/CASE WHEN d.StudyYear < 2015 THEN 1000000 ELSE 1000 END, e.Pwr_SiteFuel)
						* e.Mult_SiteFuel
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID

			
			--------E Tech--------
			UPDATE EGCTotals
			SET EGC_ETech = 
			(
				SELECT TOP 1 
					(
						CASE WHEN d.Class = 'E' THEN 
							POWER(CASE WHEN (SELECT TOP 1 Model FROM @ModelData) = 'CC' THEN d.CTG_NDC/d.CTG_Count ELSE d.CTG_NDC END, e.Pwr_ETech)
						ELSE 0 END
						* e.Mult_ETech
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID

		
			--------F Tech--------
			UPDATE EGCTotals
			SET EGC_FTech = 
			(
				SELECT TOP 1 
					(
				CASE WHEN d.Class = 'F' THEN 
					POWER(CASE (SELECT TOP 1 Model FROM @ModelData) WHEN 'CC' THEN d.CTG_NDC/d.CTG_Count WHEN 'SC' THEN CASE d.StudyYear WHEN 2013 THEN 0 ELSE d.CTG_NDC END ELSE 0 END, e.Pwr_FTech)
				ELSE 0 END
				* e.Mult_FTech
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID


			--------Aero Tech--------
			UPDATE EGCTotals
			SET EGC_AeroTech = 
			(
				SELECT TOP 1 
					(
						CASE WHEN d.Class = 'Aero' THEN 
							POWER(
								CASE WHEN d.StudyYear = 2013 THEN 
									CASE WHEN (SELECT TOP 1 Model FROM @ModelData) = 'SC' THEN d.CTG_NDC ELSE 0 END 
								ELSE 
									CASE (SELECT TOP 1 Model FROM @ModelData) WHEN 'SC' THEN d.CTG_NDC WHEN 'CC' THEN d.CTG_NDC/d.CTG_Count ELSE 0 END --change 2014
								END, e.Pwr_AeroTech)
						ELSE 0 END
						* e.Mult_AeroTech
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID


			--------Num STs--------
			UPDATE EGCTotals
			SET EGC_NumSTs = 
			(
				SELECT TOP 1 
					(
						(SELECT TOP 1 CC FROM @ModelData) 
						* POWER(d.NumSTs, e.Pwr_NumSTs)
						* e.Mult_NumSTs
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID
			

			--------Num Gens--------
			UPDATE EGCTotals
			SET EGC_NumGens = 
			(
				SELECT TOP 1 
					(
				(SELECT TOP 1 CC FROM @ModelData) 
				* POWER(CASE WHEN d.SingleShaft = 1 THEN d.CTG_Count ELSE (d.CTG_Count + d.NumSTs) END, e.Pwr_NumGens)
				* e.Mult_NumGens
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID
			
		
			--------Tot Equip--------
			UPDATE EGCTotals
			SET EGC_TotEquip = 
			(
				SELECT TOP 1 
					(
				CASE WHEN d.StudyYear < 2015 THEN 
						(SELECT TOP 1 SC FROM @ModelData)
						* POWER(
							CASE d.TotNumMajEqpt
								WHEN 6 THEN 3.45787518340236
								WHEN 8 THEN 4.76322719381692
								ELSE 2 END
							,e.Pwr_TotEquip)
						* e.Mult_TotEquip
				ELSE
					POWER(d.TotNumMajEqpt, e.Pwr_TotEquip)
						* e.Mult_TotEquip
				END
					)
				FROM @dataGrab d
				FULL JOIN PowerGlobal.dbo.EGCCoefficients e ON e.[Year] = d.StudyYear
				AND e.egcTechGroupID = (SELECT TOP 1 Model FROM @ModelData)
				AND e.ExceptionName = @exceptionName
			)
			WHERE ID = @sessionID
	END
	

	
	----2014 addition, CE units were skewing too high so we threw in a fudge factor
	--IF @year = 2014
	--BEGIN
	--	SET @EGC_Total = @EGC_Total * @IsCE
	--END

	----2015 addition, CG units were skewing too low so we threw in a fudge factor
	----IF @year = 2015
	--IF @year >= 2015
	--BEGIN
	--	SET @EGC_Total = @EGC_Total / @IsCG
	--	SET @EGC_Total = @EGC_Total / @IsSC
	--	SET @EGC_Total = @EGC_Total / @IsESC
	--END


	IF @debug = 1
	BEGIN
		
		SELECT * FROM @dataGrab
		SELECT * FROM @ModelData
		SELECT * FROM EGCTotals

	END


	------next, we're just doing a straight rip of the old code to populate the appropriate variables, so we can insert them
	--IF @debug = 0 --note that debug can be set to 1 at the start if you just want to look at the totals, not populate the database with it
	--BEGIN
	
					

	--	-- Go UPDATE all of the EGC fields spread thoughout the database.
	--	UPDATE GenerationTotCalc SET EGC = @EGC_Total WHERE Refnum = @Refnum
	--	UPDATE Gensum SET EGC = @EGC_Total WHERE Refnum = @Refnum

	--	UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnNonOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @EGC_Total > 0 THEN TotAnnOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

	--	UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHCostKUS/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnNonOHCostKUS/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnMaintCostKUS/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

	--	UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnNonOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnMaintCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHProjCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHExclProj/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @EGC_Total > 0 THEN AnnMaintExclProj/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @EGC_Total > 0 THEN AnnLTSACost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

	--	UPDATE Pers SET TotEffPersEGC = CASE WHEN @EGC_Total > 0 THEN TotEffPers*1000/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
	--	UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @EGC_Total > 0 THEN TotEffPers*1000/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

	--	UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

	--	EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @EGC_Total

	--END 

	

	--SELECT SUM(*) FROM EGCTotals