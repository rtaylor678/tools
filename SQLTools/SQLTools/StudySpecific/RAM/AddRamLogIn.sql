﻿

DECLARE
@Password VARCHAR(50) = 'QX1D=='
, @CompanyID VARCHAR(50) = '[COMPANY ID]'
, @SecurityLevel INT = 32001
, @FirstName VARCHAR(50) = '[FIRST NAME]'
, @LastName VARCHAR(50) = '[LAST NAME]'
, @Email VARCHAR(100) = '[EMAIL]'
, @JobTitle VARCHAR(50) = NULL
, @PhoneNo VARCHAR(50) = NULL
, @ScreenName VARCHAR(50) = NULL
, @UserID VARCHAR(50)



SELECT @ScreenName = @FirstName + '.' + @LastName
, @UserID = @FirstName + '.' + @LastName + '/' + @CompanyID



INSERT LogIn
(
	UserID
	, CompanyID
	, [Password]
	, SecurityLevel
	, UserLanguage
	, FirstName
	, LastName
	, Email
	, JobTitle
	, PhoneNo
	, ScreenName
	, Active
)
SELECT
	@UserID
	, @CompanyID
	, @Password
	, @SecurityLevel
	, 'en'
	, @FirstName
	, @LastName
	, @Email
	, @JobTitle
	, @PhoneNo
	, @ScreenName
	, 1