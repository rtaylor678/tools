﻿/***********************************************************************************
Description:      
As of 08 Aug 2017 this table is not in use.  The purpose of this table is to store 
results from running the EGC calcs.  It is to provide better analysis and debugging.
It also maintains history of past runs.  There is a trigger intended to work on this
table ***** trg_EGCTotals_SumEGC_.sql *****

Author: Rodrick Blanton
Date: 03 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/



	--CREATE TABLE dbo.EGCTotals 
	--(
	--	ID UNIQUEIDENTIFIER NOT NULL
	--	, StudyYear INT NOT NULL
	--	,  RefNum VARCHAR(50) NOT NULL
	--	,  EGC_Total REAL DEFAULT(0) NOT NULL
	--	,  EGC_Scrubber REAL DEFAULT(0) NOT NULL
	--	,  EGC_PrecBag REAL DEFAULT(0) NOT NULL
	--	,  EGC_SCR REAL DEFAULT(0) NOT NULL
	--	,  EGC_Frequency50 REAL DEFAULT(0) NOT NULL
	--	,  EGC_Frequency60 REAL DEFAULT(0) NOT NULL
	--	,  EGC_Starts REAL DEFAULT(0) NOT NULL
	--	,  EGC_SiteEffect REAL DEFAULT(0) NOT NULL
	--	,  EGC_OtherSiteCap REAL DEFAULT(0) NOT NULL
	--	,  EGC_SteamSales REAL DEFAULT(0) NOT NULL
	--	,  EGC_SolidFuel REAL DEFAULT(0) NOT NULL
	--	,  EGC_NatGasH2Gas REAL DEFAULT(0) NOT NULL
	--	,  EGC_OffGas REAL DEFAULT(0) NOT NULL
	--	,  EGC_DieselJet REAL DEFAULT(0) NOT NULL
	--	,  EGC_HeavyFuel REAL DEFAULT(0) NOT NULL
	--	,  EGC_Age REAL DEFAULT(0) NOT NULL
	--	,  EGC_EquivBoiler REAL DEFAULT(0) NOT NULL
	--	,  EGC_Supercritical REAL DEFAULT(0) NOT NULL
	--	,  EGC_CTGGas REAL DEFAULT(0) NOT NULL
	--	,  EGC_SiteFuel REAL DEFAULT(0) NOT NULL
	--	,  EGC_ETech REAL DEFAULT(0) NOT NULL
	--	,  EGC_FTech REAL DEFAULT(0) NOT NULL
	--	,  EGC_AeroTech REAL DEFAULT(0) NOT NULL
	--	,  EGC_NumSTs REAL DEFAULT(0) NOT NULL
	--	,  EGC_NumGens REAL DEFAULT(0) NOT NULL
	--	,  EGC_TotEquip REAL DEFAULT(0) NOT NULL
	--	,  EGC_Subcritical REAL DEFAULT(0) NOT NULL
	--	,  EGC_MWH REAL DEFAULT(0) NOT NULL
	--	,  EGC_Taxes REAL DEFAULT(0) NOT NULL
	--	,  EGC_POHours REAL DEFAULT(0) NOT NULL
	--	,  EGC_ServiceHours REAL DEFAULT(0) NOT NULL
	--	,  EGC_HRSGCap REAL DEFAULT(0) NOT NULL
	--	,  EGC_HRSGCapN REAL DEFAULT(0) NOT NULL
	--	,  EGC_CTGLiquidFuel REAL DEFAULT(0) NOT NULL
	--)


	--ALTER TABLE EGCTotals 
	--ADD createdDT DATETIME DEFAULT(GETDATE()) NOT NULL

	--ALTER TABLE EGCTotals 
	--ADD Debug BIT DEFAULT(0) NOT NULL

	--ALTER TABLE EGCTotals
	--ADD EGC_CRVGroupBiasRemoval REAL DEFAULT(1) NOT NULL

	--ALTER TABLE EGCTotals
	--DROP COLUMN EGC_CRVBias


	--ALTER TABLE EGCTotals
	----ADD EGC_Multiplier REAL DEFAULT(1) NOT NULL
	--ADD EGC_Diviser REAL DEFAULT(1) NOT NULL