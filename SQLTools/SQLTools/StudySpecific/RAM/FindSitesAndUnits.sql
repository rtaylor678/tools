﻿/***********************************************************************************
Description:      
This query is for finding Sites and Units in the Ram Study.  Using the company
name you can find the datasetID for the company and use that to find sites and 
units

Author: Rodrick Blanton
Date: 07 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/



DECLARE @CompanyName VARCHAR(50) = '[COMPANYNAME]%'
, @CompanyDatasetID INT = [COMPANYDATASETID]



/*
	This commented code is simply for finding out what the available datalevels are.
*/
--SELECT DISTINCT DataLevel
--FROM Dataset_LU




SELECT *
FROM Dataset_LU
WHERE DataLevel = 'COMP'
AND FacilityName LIKE @CompanyName



SELECT a.*
FROM 
(
	SELECT dlu.DatasetID [grp_ID]
	,  dlu.*
	FROM Dataset_LU dlu
	WHERE dlu.DataLevel = 'SITE'
	AND dlu.ParentID = @CompanyDatasetID
	UNION ALL
	SELECT dlu2.DatasetID [grp_ID]
	, dlu1.*
	FROM Dataset_LU dlu1
	INNER JOIN Dataset_LU dlu2 ON dlu1.ParentID = dlu2.DatasetID
		AND dlu2.DataLevel = 'SITE'
		AND dlu2.ParentID = @CompanyDatasetID
	WHERE dlu1.DataLevel = 'UNIT'
) AS a
ORDER BY grp_ID
	, DataLevel